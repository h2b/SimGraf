organization := "de.h2b.scala.lib"

organizationName := "private"

organizationHomepage := Some(url("http://h2b.de"))

homepage := Some(url("http://h2b.de"))

startYear := Some(2016)

description := """This is a Scala library of simple graphics utilities.
"""

licenses := Seq("Apache License, Version 2.0" -> url("https://www.apache.org/licenses/LICENSE-2.0"))

pomExtra := Seq(
	<scm>
		<url>scm:git:https://gitlab.com/h2b/SimGraf.git</url>
	</scm>,
	<developers>
		<developer>
			<id>h2b</id>
			<name>Hans-Hermann Bode</name>
			<email>projekte@h2b.de</email>
			<url>http://h2b.de</url>
			<roles>
				<role>Owner</role>
				<role>Architect</role>
				<role>Developer</role>
			</roles>
			<timezone>Europe/Berlin</timezone>
		</developer>
	</developers>
)
