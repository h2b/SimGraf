object readme {

  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  import de.h2b.scala.lib.simgraf._
  import de.h2b.scala.lib.simgraf.shapes._
  
    val w = World(Point(-100,-100), Point(100,100))(Pixel(0,0), 200, 200, "Circle")
                                                  //> w  : de.h2b.scala.lib.simgraf.World = de.h2b.scala.lib.simgraf.World$$anon$1
                                                  //| @7e2d773b/

    w.clear(Color.White)
    w.activeColor = Color.Magenta
    w.fill(Circle(Point(0,0), 75.0))

}