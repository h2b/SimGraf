/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.demo

import scala.util.{ Failure, Random, Success }

import de.h2b.scala.lib.simgraf.{ Color, Pixel, Screen }
import de.h2b.scala.lib.simgraf.layout.GridLayout

object ScreenDemo extends App {

  private val w = 300
  private val h = 200
	private val p0 = Pixel(w/2, h/2)

	private val layout = GridLayout.onScreen(2, 3).toSeq
	private def cell (i: Int) = layout(i-1).fit(w, h)

  private var n = 0

  {
    n +=1
	  val screen = Screen(cell(n), s"Screen Demo $n")
	  screen.clear(Color.White)
	  screen.activeColor = Color.Gray
	  screen.drawGrid(40, 20)
	  screen.activeColor = Color.Green
	  screen.drawCross(p0, 10)
	  screen.activeColor = Color.Cyan
	  screen.drawSquare(p0, 150)
	  screen.activeColor = Color.Magenta
	  screen.drawCircle(p0, 75)
	  screen.activeColor = Color.Orange
	  screen.drawEllipse(p0, 150, 75)
  }

  {
    n +=1
	  val screen = Screen(cell(n), s"Screen Demo $n")
	  screen.clear(Color.White)
	  screen.activeColor = Color.Gray
	  screen.drawGrid(40, 20)
	  screen.activeColor = Color.Green
	  screen.drawCross(p0, 5)
	  screen.activeColor = Color.Cyan
	  screen.fillSquare(p0, 150)
	  screen.activeColor = Color.Magenta
	  screen.fillCircle(p0, 75)
	  screen.activeColor = Color.Orange
	  screen.fillEllipse(p0, 150, 75)
	  screen.activeColor = Color.Green
	  screen.drawCross(p0, 10)
  }

  {
    n +=1
	  val screen = Screen(cell(n), s"Screen Demo $n")
	  screen.setPixels(_ => Color(Random.nextInt(256), Random.nextInt(256), Random.nextInt(256)))
  }

  {
    n +=1
	  val screen = Screen(cell(n), s"Screen Demo $n")
	  val future = screen.setPixelsParallel(_ => Color(Random.nextInt(256), Random.nextInt(256), Random.nextInt(256)))
	  future.onComplete {
      case Success(_) ⇒ println("screen.setPixelsParallel succeeded.")
      case Failure(f) ⇒ println("screen.setPixelsParallel failed with: " + f.getMessage)
    } (scala.concurrent.ExecutionContext.global)
  }

  {
    n +=1
	  val screen = Screen(cell(n), s"Screen Demo $n")
	  screen.clear(Color.White)
	  screen.activeColor = Color.Black
	  val x1 = 50
	  val x2 = w/2
	  val x3 = w-50
	  val y1 = 25
	  val y2 = h/2
	  val y3 = h-25
	  screen.moveTo(Pixel(x1,y1))
	  screen.drawTo(Pixel(x3,y1))
	  screen.drawTo(Pixel(x1,y2))
	  screen.drawTo(Pixel(x3,y2))
	  screen.drawTo(Pixel(x1,y1))
	  screen.drawTo(Pixel(x1,y2))
	  screen.drawTo(Pixel(x2,y3))
	  screen.drawTo(Pixel(x3,y2))
	  screen.drawTo(Pixel(x3,y1))
  }

}
