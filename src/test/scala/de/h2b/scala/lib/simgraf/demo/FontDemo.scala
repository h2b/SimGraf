/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.demo

import de.h2b.scala.lib.simgraf.{ Color, Pixel, Screen }
import de.h2b.scala.lib.simgraf.layout.GridLayout
import de.h2b.scala.lib.simgraf.Color._
import de.h2b.scala.lib.simgraf.shapes._
import de.h2b.scala.lib.simgraf.Font

object FontDemo extends App {

  private val testChar = 'X'
  private val testString = "abc"
  private val testText = testChar + testString + testChar

  private val iterator = GridLayout.onScreen(1, 4).iterator
  private def next () = iterator.next().fit(200, 200)

  private def newScreen (title: String) =
    Screen(next(),  title)

  {
    val screen = newScreen("Normal")
    screen.clear(Color.White)
    screen.activeColor = Color.Black
    val f0 = screen.activeFont
    var p = Pixel(10, 50)
    println("Default:   \tX→" + screen.activeFont.dimension(testChar) +
        " \tabc→" + screen.activeFont.dimension(testString))
    for (s ← Font.Size.values) {
      screen.activeFont = f0.derived(Font(s))
      println(s + ":   \tX→" + screen.activeFont.dimension(testChar) +
          " \tabc→" + screen.activeFont.dimension(testString))
      screen.drawText(p, testText)
      p += Pixel(0, 10+screen.activeFont.dimension(testText).height)
    }
  }

  for (emph ← Seq(Set(Font.Emphasis.Bold), Set(Font.Emphasis.Italic),
      Set(Font.Emphasis.Bold, Font.Emphasis.Italic))) {
    val screen = newScreen(emph.toString())
    screen.clear(Color.White)
    screen.activeColor = Color.Black
    val f0 = screen.activeFont
    var p = Pixel(10, 50)
    for (s ← Font.Size.values) {
      screen.activeFont = f0.derived(Font(s, emph))
      screen.drawText(p, testText)
      p += Pixel(0, 10+screen.activeFont.dimension(testText).height)
    }
  }

}
