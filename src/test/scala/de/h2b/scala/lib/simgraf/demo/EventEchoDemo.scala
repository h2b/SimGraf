/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.demo

import de.h2b.scala.lib.simgraf.{ Color, Point, World }
import de.h2b.scala.lib.simgraf.event.{ Event, KeyEvent, Subscriber, system }
import de.h2b.scala.lib.simgraf.layout.GridLayout
import de.h2b.scala.lib.simgraf.shapes.{ Coloring, Rectangle }

object EventEchoDemo extends App {

  private val iterator = GridLayout.onScreen(1, 2).iterator

  val frame = Rectangle(Point(-100,-100), Point(100,100))
  val cell1 = iterator.next().fit(400, 400)
  val title1 = "Event Echo demo"
  val cell2 = iterator.next().fit(400, 400)
  val title2 = "Event Echo demo"

  val world1 = World.withEvents(frame)(cell1, title1)
  world1.fill(Coloring({
    case Point(x, y) if (x>0 && y>0) ⇒ Color.Blue
    case Point(x, y) if (x>0 && y<0) ⇒ Color.Black
    case Point(x, y) if (x<0 && y>0) ⇒ Color.Green
    case Point(x, y) if (x<0 && y<0) ⇒ Color.White
    case _ ⇒ Color.Red
 }))

  val world2 = World.withEvents(frame)(cell2, title2)
  world2.fill(Coloring({
    case Point(x, y) if (x>0 && y>0) ⇒ Color.Magenta
    case Point(x, y) if (x>0 && y<0) ⇒ Color.Gray
    case Point(x, y) if (x<0 && y>0) ⇒ Color.Cyan
    case Point(x, y) if (x<0 && y<0) ⇒ Color.Yellow
    case _ ⇒ Color.Red
 }))

  Subscriber.to(world1) {
    case e: Event ⇒
      println("w1: " + e)
  }

  Subscriber.to(world2) {
    case e: Event ⇒
      println("w2: " + e)
  }

  Subscriber.to(world1, world2) {
    case KeyEvent(k) if k=='q' ⇒
      world1.screen.close()
      world2.screen.close()
      println("terminating...")
      system.terminate()
    case _: Event ⇒ ()
  }

  Subscriber.ref {
    case e: Event ⇒
      println("unexpected: " + e) //should not happen
  }

}
