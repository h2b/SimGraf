/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import de.h2b.scala.lib.simgraf.Color._

@RunWith(classOf[JUnitRunner])
class ColorIteratorTest extends FunSuite {

  private val fullSeq = Seq(Black, Blue, Red, Green, Gray, Cyan, Magenta, Yellow,
	    DarkGray, Orange, Pink, LightGray, White)
	private val n = fullSeq.length

	//doing double loops to test cycling

  test("iteration over some seq") {
    val seq = Seq(Red, Green, Blue)
    val it = ColorIterator.over(Red, Green, Blue)
    for (i ← 0 until seq.length) assertResult(seq(i))(it.next())
    for (i ← 0 until seq.length) assertResult(seq(i))(it.next())
  }

  test("iteration over full seq") {
    val it = ColorIterator.excluding()
    for (i ← 0 until n) assertResult(fullSeq(i))(it.next())
    for (i ← 0 until n) assertResult(fullSeq(i))(it.next())
  }

  test("iteration over full seq excluding black") {
    val exclude = Black
    val it = ColorIterator.excluding(exclude)
    for (i ← 0 until n if fullSeq(i)!=exclude) assertResult(fullSeq(i))(it.next())
    for (i ← 0 until n if fullSeq(i)!=exclude) assertResult(fullSeq(i))(it.next())
  }

  test("iteration over full seq excluding black and gray") {
    val excludes = Set(Black, Gray)
    val it = ColorIterator.excluding(excludes.toSeq: _*)
    for (i ← 0 until n if !excludes.contains(fullSeq(i))) assertResult(fullSeq(i))(it.next())
    for (i ← 0 until n if !excludes.contains(fullSeq(i))) assertResult(fullSeq(i))(it.next())
  }

  test("toSeq") {
    assertResult(fullSeq)(ColorIterator.excluding().toSeq)
    assertResult(fullSeq.diff(Seq(Red)))(ColorIterator.excluding(Red).toSeq)
    assertResult(fullSeq.diff(Seq(Cyan, Magenta, Yellow)))(ColorIterator.excluding(Cyan, Magenta, Yellow).toSeq)
  }

}
