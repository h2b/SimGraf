/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.demo

import scala.concurrent.Future
import scala.math.{ Pi, sin }
import scala.util.{ Failure, Random, Success }

import de.h2b.scala.lib.simgraf.{ Color, Point, World }
import de.h2b.scala.lib.simgraf.layout.GridLayout
import de.h2b.scala.lib.simgraf.shapes.{ Grid, Rectangle }
import de.h2b.scala.lib.simgraf.shapes.parallel._

object ParallelShapesDemo extends App {

  private val p1 = Point(-100,-100)
  private val p2 = Point(100,100)
  private val p0 = p1+(p2-p1)*0.5

  private val w = 300
  private val h = 300

  private val iterator = GridLayout.onScreen(2, 3).iterator
  private def next () = iterator.next().fit(w, h)

  private def newDefaultWorld (title: String) =
    World(Rectangle(p1, p2))(next(),  title)
  private def newSinusWorld (title: String) =
    World(Rectangle(Point(-2*Pi,-1.2), Point(+2*Pi,+1.2)))(next(),  title)

  private def onComplete (fut: Future[_], name: String) = fut.onComplete {
    case Success(_) ⇒ println(s"$name succeeded.")
    case Failure(f) ⇒ println(s"$name failed with: " + f.getMessage)
  } (scala.concurrent.ExecutionContext.global)

  {
    val name = "Parallel Random Coloring"
    val world = newDefaultWorld(name)
    val f = Coloring(_ =>
	    Color(Random.nextInt(256), Random.nextInt(256), Random.nextInt(256)))
	  world.fill(f)
    onComplete(f.currentFuture, name)
  }

  {
    val name = "Parallel Drawing y=sin(x)"
    val world = newSinusWorld(name)
    world.clear(Color.White)
    world.activeColor = Color.Gray
    world.draw(Grid(Pi/2, 0.2))
    world.activeColor = Color.Red
    val f = Function((x: Double) ⇒ sin(x))
    world.draw(f)
    onComplete(f.currentFuture, name)
  }

  {
    val name = "Parallel Filling y=sin(x)"
    val world = newSinusWorld(name)
    world.clear(Color.White)
    world.activeColor = Color.Gray
    world.draw(Grid(Pi/2, 0.2))
    world.activeColor = Color.Blue
    val f = Function((x: Double) ⇒ sin(x))
    world.fill(f)
    onComplete(f.currentFuture, name)
  }

  private def fuzzySinus (x: Double) = {
    val y = sin(x)
    val n = 5
    var result = Set.empty[Double]
    for (i ← 1 to n) result += y+(0.5-Random.nextDouble())/2.5
    result
  }

  {
    val name = "Parallel Drawing Fuzzy Sinus"
    val world = newSinusWorld(name)
    world.clear(Color.Yellow)
    world.activeColor = Color.Orange
    world.draw(Grid(Pi/2, 0.2))
    world.activeColor = Color.Magenta
    val f = MultivaluedFunction(fuzzySinus)
    world.draw(f)
    onComplete(f.currentFuture, name)
  }

  {
    val name = "Parallel Filling Fuzzy Sinus"
    val world = newSinusWorld(name)
    world.clear(Color.Yellow)
    world.activeColor = Color.Orange
    world.draw(Grid(Pi/2, 0.2))
    world.activeColor = Color.Cyan
    val f = MultivaluedFunction(fuzzySinus)
    world.fill(f)
    onComplete(f.currentFuture, name)
  }

}
