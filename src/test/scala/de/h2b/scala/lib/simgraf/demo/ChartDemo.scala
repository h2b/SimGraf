/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.demo

import de.h2b.scala.lib.simgraf.{ Color, Point, World }
import de.h2b.scala.lib.simgraf.layout.GridLayout
import de.h2b.scala.lib.simgraf.Color._
import de.h2b.scala.lib.simgraf.shapes._

object ChartDemo extends App {

  private val p1Bar = Point(0,-30)
  private val p2Bar = Point(100,100)

  private val p1Pie = Point(-100,-100)
  private val p2Pie = Point(100,100)

  private val iterator = GridLayout.onScreen(1, 4).iterator
  private def next () = iterator.next().fit(200, 200)

  private def newDefaultBarWorld (title: String) =
    World(Rectangle(p1Bar, p2Bar))(next(),  title)

  private def newDefaultPieWorld (title: String) =
    World(Rectangle(p1Pie, p2Pie))(next(),  title)

  private def colorSeq = Seq(Red, Green, Blue, Yellow, Cyan)

  {
    val world = newDefaultBarWorld("Bar Chart Drawed")
    world.clear(Color.Black)
    world.activeColor = Color.Black
    world.draw(BarChart(Seq(30, 60, 20, 80, 50), colorSeq))
  }

  {
    val world = newDefaultBarWorld("Bar Chart Filled")
    world.clear(Color.Black)
    world.activeColor = Color.Black
    world.fill(BarChart(Seq(30, 60, 20, 80, 50), colorSeq))
  }

  {
    val world = newDefaultPieWorld("Pie Chart Drawed")
    world.clear(Color.Black)
    world.activeColor = Color.Black
    world.draw(PieChart(Seq(30, 60, 20, 80, 50), colorSeq))
  }

  {
    val world = newDefaultPieWorld("Pie Chart Filled")
    world.clear(Color.Black)
    world.activeColor = Color.Black
    world.fill(PieChart(Seq(30, 60, 20, 80, 50), colorSeq))
  }

}
