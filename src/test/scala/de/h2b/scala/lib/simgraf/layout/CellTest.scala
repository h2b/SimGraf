/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.layout

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.scala.lib.simgraf.Pixel

import Cell.HorizontalOrientation._
import Cell.VerticalOrientation._

@RunWith(classOf[JUnitRunner])
class CellTest extends FunSuite {

  val cell = Cell(1000, 1000)

  test("at") {
    assertResult(Cell(1000, 1000, Pixel(100, 200)))(cell at Pixel(100, 200))
  }

  test("fitting default orientation") {
    assertResult(Cell(100, 100, Pixel(450, 450)))(cell.fit(100, 100))
  }

  test("fitting default orientation with shifted origin") {
    assertResult(Cell(100, 100, Pixel(550, 650)))(cell.at(Pixel(100, 200)).fit(100, 100))
  }

  test("fitting arbitrary orientations") {
    assertResult(Cell(100, 100, Pixel(0, 0)))(cell.fit(100, 100, Cell.Orientation(Left, Top)))
    assertResult(Cell(100, 100, Pixel(450, 450)))(cell.fit(100, 100, Cell.Orientation(HCenter, VCenter)))
    assertResult(Cell(100, 100, Pixel(900, 900)))(cell.fit(100, 100, Cell.Orientation(Right, Bottom)))
  }

  test("fitting arbitrary orientations with shifted origin") {
    val shifted = cell at Pixel(100, 200)
    assertResult(Cell(100, 100, Pixel(100, 200)))(shifted.fit(100, 100, Cell.Orientation(Left, Top)))
    assertResult(Cell(100, 100, Pixel(550, 650)))(shifted.fit(100, 100, Cell.Orientation(HCenter, VCenter)))
    assertResult(Cell(100, 100, Pixel(1000, 1100)))(shifted.fit(100, 100, Cell.Orientation(Right, Bottom)))
  }

}
