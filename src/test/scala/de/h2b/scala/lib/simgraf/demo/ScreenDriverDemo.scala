/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.demo

import scala.util.Random

import de.h2b.scala.lib.simgraf.{ Color, Pixel }
import de.h2b.scala.lib.simgraf.driver.ScreenDriver
import de.h2b.scala.lib.simgraf.layout.GridLayout
import de.h2b.scala.lib.simgraf.Screen
import de.h2b.scala.lib.simgraf.driver.AwtScreenDriver
import de.h2b.scala.lib.simgraf.layout.Cell

object ScreenDriverDemo extends App {

  private val w = 300
  private val h = 200

  private val iterator = GridLayout.onScreen(2, 3).iterator
  private def next () = iterator.next().fit(w, h)

  private def driver (cell: Cell, title: String): ScreenDriver =
    new ScreenDriver(cell.origin, cell.width, cell.height, title) with AwtScreenDriver

  private val driver1 = driver(next(), "Screen Driver Demo 1")
  drawGrid(driver1)
  driver1.activeColor = Color.Blue
  driver1.drawLine(Pixel(0,0), Pixel(w,h))
  driver1.activeColor = Color.Red
  driver1.drawLine(Pixel(0,h), Pixel(w,0))

  private val driver2 = driver(next(), "Screen Driver Demo 2")
  drawGrid(driver2)
  driver2.activeColor = Color.Blue
  driver2.fillRectangle(Pixel(50,50), Pixel(w-50,h-50))
  driver2.activeColor = Color.Red
  driver2.drawRectangle(Pixel(50,50), Pixel(w-50,h-50))

  private val driver3 = driver(next(), "Screen Driver Demo 3")
  drawGrid(driver3)
  val poly3 = Seq(Pixel(50,50), Pixel(w-50,50), Pixel(w/2,h-50), Pixel(50,50))
  driver3.activeColor = Color.Blue
  driver3.fillPolygon(poly3)
  driver3.activeColor = Color.Red
  driver3.drawPolyline(poly3)

  private val driver4 = driver(next(), "Screen Driver Demo 4")
  drawGrid(driver4)
  driver4.activeColor = Color.Blue
  driver4.fillArc(Pixel(w/2,h/2), w-100, h-100, 0, 90)
  driver4.activeColor = Color.Yellow
  driver4.fillArc(Pixel(w/2,h/2), w-100, h-100, 90, 270)
  driver4.activeColor = Color.Red
  driver4.drawArc(Pixel(w/2,h/2), w-100, h-100, 0, 360)

  private val driver5 = driver(next(), "Screen Driver Demo 5")
  for {
	  x ← 0 to w
	  y ← 0 to h
  } {
    val col = Color(Random.nextInt(256), Random.nextInt(256), Random.nextInt(256))
	  driver5.setPixel(Pixel(x,y), col)
  }

  private def drawGrid (driver: ScreenDriver): Unit = {
	  val prevCol = driver.activeColor
	  driver.activeColor = Color.Green
	  for (x ← 0 to w by 40) {
		  driver.drawText(Pixel(x, 0), x.toString())
		  driver.drawLine(Pixel(x, 0), Pixel(x, h))
	  }
	  for (y ← 0 to h by 20) {
		  driver.drawText(Pixel(0, y), y.toString())
		  driver.drawLine(Pixel(0, y), Pixel(w, y))
	  }
	  driver.activeColor = prevCol
  }

}
