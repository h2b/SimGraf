/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.demo

import scala.math.{ Pi, sin }
import scala.util.Random

import de.h2b.scala.lib.simgraf.{ Color, Pixel, Point, Screen, World }
import de.h2b.scala.lib.simgraf.layout.GridLayout
import de.h2b.scala.lib.simgraf.shapes._

object WorldDemo extends App {

  private val p1 = Point(-100,-100)
  private val p2 = Point(100,100)
  private val p0 = p1+(p2-p1)*0.5

  private val w = 200
  private val h = 200

  private val iterator = GridLayout.onScreen(3, 5).iterator
  private def next () = iterator.next().fit(w, h)

  private def newDefaultWorld (title: String) =
    World(Rectangle(p1, p2))(next(),  title)
  private def newSinusWorld (title: String) =
    World(Rectangle(Point(-2*Pi,-1.2), Point(+2*Pi,+1.2)))(next(),  title)

  {
    val world = newDefaultWorld("Random Points")
    world.clear(Color.Black)
    val count = 1000
    for (i ← 1 to count) {
      val x = Random.nextDouble()*(p2.x-p1.x)
      val y = Random.nextDouble()*(p2.y-p1.y)
    	world.activeColor = Color(Random.nextInt(256), Random.nextInt(256), Random.nextInt(256))
      world.plot(p1+Point(x,y))
    }
  }

  {
    val world = newDefaultWorld("Lines")
    world.clear(Color.White)
    world.activeColor = Color.Gray
    world.draw(Grid(50, 50))
    world.activeColor = Color.Blue
    world.draw(Line(p1, p2))
    world.activeColor = Color.Red
    world.draw(Line(Point(p2.x,p1.y), Point(p1.x,p2.y)))
  }

  {
    val world = newDefaultWorld("Rectangle")
    world.clear(Color.White)
    world.activeColor = Color.Gray
    world.draw(Grid(50, 50))
    world.activeColor = Color.Blue
    world.fill(Rectangle(Point(-50,-50), Point(50,50)))
    world.activeColor = Color.Red
    world.draw(Rectangle(Point(-50,-50), Point(50,50)))
  }

  {
    val world = newDefaultWorld("Polygonal")
    world.clear(Color.White)
    world.activeColor = Color.Gray
    world.draw(Grid(50, 50))
    val triag = Seq(Point(-50,-50), Point(50,-50), Point(0,50), Point(-50,-50))
    world.activeColor = Color.Blue
    world.fill(Polygonal(triag))
    world.activeColor = Color.Red
    world.draw(Polygonal(triag))
  }

  {
    val world = newDefaultWorld("Arcs")
    world.clear(Color.White)
    world.activeColor = Color.Gray
    world.draw(Grid(50, 50))
    val d1 = 150.0
    val d2 = 100.0
    world.activeColor = Color.Blue
    world.fill(Arc(p0, d1, d2, 0, Pi/2))
    world.activeColor = Color.Yellow
    world.fill(Arc(p0, d1, d2, Pi/2, 3*Pi/2))
    world.activeColor = Color.Red
    world.draw(Arc(p0, d1, d2, 0, 2*Pi))
  }

  {
    val world = newDefaultWorld("Drawing Shapes")
    world.clear(Color.White)
    world.activeColor = Color.Gray
    world.draw(Grid(50, 50))
    val d = 150.0
	  world.activeColor = Color.Green
	  world.draw(Plus(p0, 10))
	  world.activeColor = Color.Cyan
	  world.draw(Square(p0, d))
	  world.activeColor = Color.Magenta
	  world.draw(Circle(p0, d/2))
	  world.activeColor = Color.Orange
	  world.draw(Ellipse(p0, d, d/2))
  }

  {
    val world = newDefaultWorld("Filling Shapes")
    world.clear(Color.White)
    world.activeColor = Color.Gray
    world.draw(Grid(50, 50))
    val d = 150.0
	  world.activeColor = Color.Cyan
	  world.fill(Square(p0, d))
	  world.activeColor = Color.Magenta
	  world.fill(Circle(p0, d/2))
	  world.activeColor = Color.Orange
	  world.fill(Ellipse(p0, d, d/2))
	  world.activeColor = Color.Green
	  world.draw(Plus(p0, 15))
  }

  {
    val world = newDefaultWorld("Haus vom Nikolaus")
    world.clear(Color.White)
	  world.activeColor = Color.Black
	  val x1 = -50.0
	  val x2 = 0.0
	  val x3 = +50.0
	  val y1 = -50.0
	  val y2 = 0.0
	  val y3 = +50.0
	  world.moveTo(Point(x1,y1))
	  world.drawTo(Point(x3,y1))
	  world.drawTo(Point(x1,y2))
	  world.drawTo(Point(x3,y2))
	  world.drawTo(Point(x1,y1))
	  world.drawTo(Point(x1,y2))
	  world.drawTo(Point(x2,y3))
	  world.drawTo(Point(x3,y2))
	  world.drawTo(Point(x3,y1))
  }

  {
    val world = newDefaultWorld("Random Coloring")
	  world.fill(Coloring(_ =>
	    Color(Random.nextInt(256), Random.nextInt(256), Random.nextInt(256))))
  }

  {
    val world = newSinusWorld("Drawing y=sin(x)")
    world.clear(Color.White)
    world.activeColor = Color.Gray
    world.draw(Grid(Pi/2, 0.2))
    world.activeColor = Color.Red
    world.draw(Function((x: Double) ⇒ sin(x)))
  }

  {
    val world = newSinusWorld("Filling y=sin(x)")
    world.clear(Color.White)
    world.activeColor = Color.Gray
    world.draw(Grid(Pi/2, 0.2))
    world.activeColor = Color.Blue
    world.fill(Function((x: Double) ⇒ sin(x)))
  }

  private def fuzzySinus (x: Double) = {
    val y = sin(x)
    val n = 5
    var result = Set.empty[Double]
    for (i ← 1 to n) result += y+(0.5-Random.nextDouble())/2.5
    result
  }

  {
    val world = newSinusWorld("Drawing Fuzzy Sinus")
    world.clear(Color.Yellow)
    world.activeColor = Color.Orange
    world.draw(Grid(Pi/2, 0.2))
    world.activeColor = Color.Magenta
    world.draw(MultivaluedFunction(fuzzySinus))
  }

  {
    val world = newSinusWorld("Filling Fuzzy Sinus")
    world.clear(Color.Yellow)
    world.activeColor = Color.Orange
    world.draw(Grid(Pi/2, 0.2))
    world.activeColor = Color.Cyan
    world.fill(MultivaluedFunction(fuzzySinus))
  }

}
