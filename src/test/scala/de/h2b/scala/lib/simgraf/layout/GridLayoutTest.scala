/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.layout

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.scala.lib.simgraf.layout.Floating._
import de.h2b.scala.lib.simgraf.Pixel

@RunWith(classOf[JUnitRunner])
class GridLayoutTest extends FunSuite {

  val hlayout = GridLayout.withinCell(Cell(300, 200, Pixel(50, 150)), 2, 3)
  val vlayout = GridLayout.withinCell(Cell(300, 200), 2, 3, floating=Vertical) at Pixel(50, 150)

  val cell = Cell(100, 100)

  test("at") {
    val actual = hlayout at Pixel(100, 200)
    assertResult(Pixel(100, 200))(actual.origin)
  }

  test("horizontal iterator") {
    val expected = Seq(
        Pixel(50,150), Pixel(150,150), Pixel(250,150),
        Pixel(50,250), Pixel(150,250), Pixel(250,250))
    var i = 0
    for (p ← hlayout) {
      assertResult(cell at expected(i))(p)
      i += 1
    }
  }

  test("vertical iterator") {
    val expected = Seq(
        Pixel( 50,150), Pixel( 50,250),
        Pixel(150,150), Pixel(150,250),
        Pixel(250,150), Pixel(250,250))
    var i = 0
    for (p ← vlayout) {
      assertResult(cell at expected(i))(p)
      i += 1
    }
  }

}
