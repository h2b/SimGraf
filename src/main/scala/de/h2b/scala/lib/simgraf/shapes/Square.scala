/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.shapes

import de.h2b.scala.lib.simgraf.{ Point, World }

/**
 * A square with centre at `p0` and the specified side length `l`.
 *
 * @author h2b
 *
 * @constructor
 *
 * @param p0 centre of the square
 * @param l side length of the square
 */
case class Square (val p0: Point, val l: Double) extends Drawable with Fillable {

  private val rect = Rectangle(Point(p0.x-l/2, p0.y-l/2), Point(p0.x+l/2, p0.y+l/2))

	def draw (w: World): Unit = rect.draw(w)

  def fill (w: World): Unit = rect.fill(w)

}
