/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.driver

import de.h2b.scala.lib.simgraf.{ Color, Font, FontMetrics, Pixel }

/**
 * Driver methods for screen graphics.
 *
 * The drawing area will have the specified width and height and will be
 * positioned at the given location (if possible).
 *
 * It will have a coordinate system with the origin `(0, 0)` in the lower-left
 * corner and the upper-right corner at `(width-1, height-1)`. All operations
 * ensure clipping to that area, so that it is safe to use coordinates outside
 * of it.
 *
 * Implementations have to provide the `activeColor` and `activeFont`
 * variables and the `draw...` and `fill...` methods as well as `getPixel`
 * and `setPixel`.
 *
 * @author h2b
 *
 * @constructor
 *
 * @param p0 location of the upper-left corner on the screen
 * @param width horizontal dimension
 * @param height vertical dimension
 * @param title optional window title (will not reduce the drawing area,
 * defaults to empty string)
 */
abstract class ScreenDriver protected (val p0: Pixel, val width: Int,
    val height: Int, val title: String = "") {

	/**
	 *  color to be used for the next operations (except for those that use
	 *  their own color)
	 **/
  var activeColor: Color

	/**
	 *  font to be used for the next operations (except for those that use
	 *  their own font)
	 **/
  var activeFont: Font with FontMetrics

  /**
   * Sets pixel to color.
   *
   * @param p
   * @param color
   */
  def setPixel (p: Pixel, color: Color): Unit

  /**
   * Gets color of a pixel.
   *
   * @param p
   * @return the color of the pixel at `p` or `Color(0, 0, 0)` if it is outside
   * of the drawing area
   */
  def getPixel (p: Pixel): Color

  /**
   * Draws a line from `p1` to `p2`.
   *
   * @param p1
   * @param p2
   */
  def drawLine (p1: Pixel, p2: Pixel): Unit

  /**
   * Draws a rectangle with diagonal opposite corners at `p1` and `p2`.
   *
   * @param p1
   * @param p2
   */
  def drawRectangle (p1: Pixel, p2: Pixel): Unit

  /**
   * Fills a rectangle with diagonal opposite corners at `p1` and `p2`.
   *
   * @param p1
   * @param p2
   */
  def fillRectangle (p1: Pixel, p2: Pixel): Unit

  /**
   * Draws a polygonal chain with vertices defined by the specified sequence.
   *
   * The chain is not closed by default. To achieve this, set the last vertex
   * equal to the first.
   *
   * @param p
   */
  def drawPolyline (p: Seq[Pixel]): Unit

  /**
   * Fills a polygonal area with vertices defined by the specified sequence.
   *
   * There is no need to close the sequence of vertices.
   *
   * @param p
   */
  def fillPolygon(p: Seq[Pixel]): Unit

  /**
   * Draws an arc within a virtual rectangle.
   *
   * Angles are given in degrees, where 0 is the direction of the x axis and
   * positive values count counter-clockwise while negative values count
   * clockwise.
   *
   * @param p0 centre of the arc
   * @param width of the enclosing rectangle
   * @param height of the enclosing rectangle
   * @param startAngle start in degrees
   * @param arcAngle extent in degrees
   *
   * @example drawArc(Pixel(10,10), 5, 5, 0, 360) draws a circle of diameter 5
   * around the centre (10,10)
   */
  def drawArc (p0: Pixel, width: Int, height: Int, startAngle: Int, arcAngle: Int): Unit

  /**
   * Fills an arc within a virtual rectangle.
   *
   * Angles are given in degrees, where 0 is the direction of the x axis and
   * positive values count counter-clockwise while negative values count
   * clockwise.
   *
   * @param p0 centre of the arc
   * @param width of the enclosing rectangle
   * @param height of the enclosing rectangle
   * @param startAngle start in degrees
   * @param arcAngle extent in degrees
   *
   * @example fillArc(Pixel(10,10), 5, 5, 0, 360) fills a circle of diameter 5
   * around the centre (10,10)
   */
  def fillArc (p0: Pixel, width: Int, height: Int, startAngle: Int, arcAngle: Int): Unit

  /**
   * Draws a text with lower-left corner at `p0`.
   *
   * @param p0
   * @param text `null` does nothing
   */
  def drawText (p0: Pixel, text: String): Unit

  /**
   * Closes the screen this driver is associated to.
   *
   * @note Might not have an effect if the underlying implementation does not
   * support such an operation.
   *
   * @since 1.3.0
   */
  def close (): Unit

  /**
   * Helper function for systems with origin at upper-left corner.
   *
   * @param y original coordinate
   * @return vertical flipped coordinate
   */
  protected def flipV (y: Int) = height-y-1

}

/**
 * @note As of version 1.3.0, factory methods have been removed, since screen
 * required now.
 */
object ScreenDriver {

	/** screen origin **/
  val screenOrigin: Pixel = AwtScreenDriver.screenOrigin
	/** available screen height in pixels */
  val screenHeight: Int = AwtScreenDriver.screenHeight
	/** available screen width in pixels */
  val screenWidth: Int = AwtScreenDriver.screenWidth

}
