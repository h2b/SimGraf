/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf

/**
 * Provides an iterator over colors.
 *
 * The iterator cycles over the available color sequence, so it always has a
 * next color.
 *
 * @author h2b
 */
class ColorIterator private (val colors: Seq[Color]) extends Iterator[Color] {

  private val n = colors.size

  require(n>0, "no colors left to iterate")

  private var i = 0

  def hasNext = true

  def next (): Color = {
    i += 1
    colors((i-1)%n)
  }

  /**
   * Converts this iterator to a sequence.
   *
   * Although this iterator is of infinite size, this version of `toSeq`
   * terminates and returns the underlying color sequence cycle.
   */
  override def toSeq: Seq[Color] = colors

}

object ColorIterator {

	import de.h2b.scala.lib.simgraf.Color._

	private val stdSeq = Seq(Black, Blue, Red, Green, Gray, Cyan, Magenta, Yellow,
	    DarkGray, Orange, Pink, LightGray, White)

	@deprecated("use ColorIterator.excluding instead", "SimGraf 1.4.0")
	def apply (exclude: Color*): ColorIterator = excluding(exclude: _*)

	/**
	 * Returns an iterator over all standard colors defined in the `Color`
	 * companion object except for the ones specified.
	 *
   * The iterator cycles over the resulting color sequence, so it always has a
   * next color.
   *
	 * @note Beware, that the resulting color sequence must not be empty.
	 *
	 * @param exclude colors to be excluded from this iteration
	 * @return a new color iterator over standard colors
	 * @throws IllegalArgumentException if the resulting color sequence is empty
	 */
	def excluding (exclude: Color*): ColorIterator = {
    val colors = ColorIterator.stdSeq.diff(exclude)
    new ColorIterator(colors)
	}

	/**
	 * Returns an iterator over all specified colors.
	 *
   * The iterator cycles over the available color sequence, so it always has a
   * next color.
   *
	 * @note Beware, that the specified color sequence must not be empty.
	 *
	 * @param colors
	 * @return a new color iterator over specified colors
	 * @throws IllegalArgumentException if the color sequence is empty
	 */
	def over (colors: Color*): ColorIterator = new ColorIterator(colors)

}
