/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.shapes

import de.h2b.scala.lib.simgraf.{ Point, World }

/**
 * General base trait.
 *
 * @author h2b
 */
trait Shape {

	/**
	 * Puts this shape and the specified other shape into a collage, starting with
	 * this shape.
	 *
	 * @param other
	 * @return
	 */
	def union (other: Shape): Collage = Collage(Seq(this, other))

}

/**
 * A shape that can be drawn in a world coordinate system.
 *
 * @author h2b
 */
trait Drawable extends Shape {

	/**
	 * Draws this shape within the specified world using its active color.
	 *
	 * @param w
	 */
	def draw (w: World): Unit

}

/**
 * A shape that can be filled in a world coordinate system.
 *
 * @author h2b
 */
trait Fillable extends Shape {

	/**
	 * Fills this shape within the specified world using its active color.
	 *
	 * @param w
	 */
  def fill (w: World): Unit

}

/**
 * Defines an enclosing for some shapes.
 *
 * @author h2b
 *
 * @constructor
 *
 * @param boundssOption some enclosing rectangle or `None` for the full world
 */
class Enclosing (private val boundsOption: Option[Rectangle]) {

  private def rect (w: World): Rectangle = boundsOption match {
    case Some(b) ⇒ b
    case None ⇒ Rectangle(w.p1, w.p2)
  }

  /**
   * @param w the world we are within
   * @return the effective left, right, bottom and top bound of this enclosing
   * (in this order)
   */
  def lrbt (w: World): (Double, Double, Double, Double) = {
    val r = rect(w)
    (r.p1.x min r.p2.x, r.p1.x max r.p2.x, r.p1.y min r.p2.y, r.p1.y max r.p2.y)
  }

  /**
   * @param w the world we are within
   * @return the effective bounding rectangle of this enclosing with its `p1` as
   * lower left and its `p2` as upper right corner
   */
  def bounds (w: World): Rectangle = {
    val (l, r, b, t) = lrbt(w)
    Rectangle(Point(l, b), Point (r, t))
  }

}

object Enclosing {

  implicit val fullEnclosing = Enclosing()

  /** Creates an enclosing by the specified rectangle. */
  def apply (r: Rectangle): Enclosing = new Enclosing(Some(r))

  /** Creates an enclosing by the specified diagonal opposite corners. */
  def apply (p1: Point, p2: Point): Enclosing = new Enclosing(Some(Rectangle(p1, p2)))

  /** Creates an enclosing equal to the full world. */
  def apply (): Enclosing = new Enclosing(None)

}
