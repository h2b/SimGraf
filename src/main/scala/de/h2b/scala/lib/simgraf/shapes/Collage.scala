/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.shapes

import de.h2b.scala.lib.simgraf.World

/**
 * A collection of shapes.
 *
 * This collage can contain both drawables and fillables, but of course only
 * drawables are drawn and fillables are filled. In both cases, items are
 * processed in the order in which they appear within the sequence parameter.
 *
 * @author h2b
 */
case class Collage (val items: Seq[Shape]) extends Drawable with Fillable {

  private val drawables = items collect { case d: Drawable ⇒ d }
  private val fillables = items collect { case f: Fillable ⇒ f }

	def draw (w: World): Unit = drawables foreach { _.draw(w) }

  def fill (w: World): Unit = fillables foreach { _.fill(w) }

}
