/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.driver

import java.awt.{ Color => AwtColor, Dimension, FileDialog, Font ⇒ AwtFont,
  Frame, Graphics, GraphicsEnvironment, Rectangle, RenderingHints }
import java.awt.event.{ ActionEvent, ActionListener, WindowEvent }
import java.awt.image.BufferedImage
import java.io.{ File, IOException }

import scala.collection.JavaConverters.mapAsJavaMapConverter

import de.h2b.scala.lib.simgraf.{ Color, Font, FontMetrics, Pixel }
import de.h2b.scala.lib.util.{ Level, Logger }
import de.h2b.scala.lib.util.config.BundleConfig
import javax.imageio.ImageIO
import javax.swing.{ JFrame, JMenu, JMenuBar, JMenuItem, JPanel,
  WindowConstants }

/**
 * Screen-driver implementation trait based on AWT (and some Swing).
 *
 * Provides hooks for key and mouse listeners that can be overridden by
 * implementations. By default, no such listeners are enabled.
 *
 * Also provides a file menu with save item as bonus functionality.
 *
 * @author h2b
 */
trait AwtScreenDriver extends ScreenDriver {

  import AwtScreenDriver._
  import AwtScreenDriver.Localizer._

  private val log = Logger(getClass) at Level.Error

  private val conf = env.getDefaultScreenDevice.getDefaultConfiguration
  private val image = conf.createCompatibleImage(width, height)
  protected val graphics = image.createGraphics()

  private val rendering = Map(
      RenderingHints.KEY_ANTIALIASING → RenderingHints.VALUE_ANTIALIAS_ON,
      RenderingHints.KEY_RENDERING → RenderingHints.VALUE_RENDER_QUALITY)

  graphics.addRenderingHints(rendering.asJava)

  private var _activeColor: Color = toColor(graphics.getColor)
  def activeColor: Color = _activeColor
  def activeColor_= (col: Color): Unit = {
    _activeColor = col
    graphics.setColor(toAwtColor(_activeColor))
  }

  private def newFont (size: Font.Size.Value,
      emphasis: Set[Font.Emphasis.Value]): Font with FontMetrics = {
    val awtFont: AwtFont = graphics.getFont
    val newSizedFont = sized(awtFont, size)
    val newStyledFont = styled(newSizedFont, emphasis)
    graphics.setFont(newStyledFont)
    val f = new Font(size, emphasis) with FontMetrics {
      val metrics = graphics.getFontMetrics
      val h = metrics.getHeight
      def dimension (c: Char): Dimension = Dimension(metrics.charWidth(c), h)
      def dimension (s: String): Dimension = Dimension(metrics.stringWidth(s), h)
      def derived (font: Font): Font with FontMetrics = newFont(font.size, font.emphasis)
    }
    f
  }

  private def sized (awtFont: AwtFont, size: Font.Size.Value) =
    size match {
      case Font.Size.Tiny ⇒ awtFont.deriveFont(8f)
      case Font.Size.Small ⇒ awtFont.deriveFont(10f)
      case Font.Size.Normal ⇒ awtFont.deriveFont(12f)
      case Font.Size.Large ⇒ awtFont.deriveFont(14f)
      case Font.Size.Huge ⇒ awtFont.deriveFont(16f)
      case _ ⇒ awtFont
    }

  private def styled (awtFont: AwtFont, emphasis: Set[Font.Emphasis.Value]) = {
    var result = awtFont.deriveFont(AwtFont.PLAIN)
    emphasis foreach {
      _ match {
        case Font.Emphasis.Bold ⇒ result = result.deriveFont(AwtFont.BOLD)
        case Font.Emphasis.Italic ⇒ result = result.deriveFont(AwtFont.ITALIC)
      }
    }
    result
  }

  private var _activeFont: Font with FontMetrics =
    newFont(Font.Size.Normal, Set.empty)
  def activeFont: Font with FontMetrics = _activeFont
  def activeFont_= (font: Font with FontMetrics): Unit = {
    val f = newFont(font.size, font.emphasis)
    _activeFont = f
  }

  protected val panel = new JPanel {
    log.debug("panel entered")
    override protected def paintComponent(g: Graphics): Unit = {
      super.paintComponent(g)
      g.drawImage(image, 0, 0, null)
    }
  }

  private val frame = new JFrame {
    log.debug("frame entered")
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)
  	setLocation(p0.x, p0.y)
  	setTitle(title)
  	setResizable(false)
  	add(panel)
  	getContentPane.setPreferredSize(new Dimension(width, height))
  	pack()
  	setVisible(false)
  }

  //bonus functionality, not required by super class
  private def menuBar (): JMenuBar = {
    log.debug("menu bar entered")
    val result = new JMenuBar
    val menu = new JMenu(local(Key.File))
    val item = new JMenuItem(local(Key.SaveAs))
    item.addActionListener(new FileMenu(frame))
    menu.add(item)
    result.add(menu)
    result
  }

  frame.setJMenuBar(menuBar())
  frame.setVisible(true)

  def setPixel (p: Pixel, color: Color): Unit = {
    val x = p.x
    val y = flipV(p.y)
    if (0<=x && x<width && 0<=y && y<height && color.alpha>0) {
    	val rgb = (color.red << 16) | (color.green << 8) | color.blue
    	image.setRGB(x, y, rgb)
    	panel.repaint()
    }
  }

  def getPixel (p: Pixel): Color = {
    val x = p.x
    val y = flipV(p.y)
    if (0<=x && x<width && 0<=y && y<height) {
    	val rgb = image.getRGB(x, y)
    	val r = (rgb >> 16) & 0xff
    	val g = (rgb >> 8) & 0xff
    	val b = rgb & 0xff
    	Color(r, g, b)
    } else
      Color(0, 0, 0)
  }

  def drawLine (p1: Pixel, p2: Pixel): Unit = {
    graphics.drawLine(p1.x, flipV(p1.y), p2.x, flipV(p2.y))
    panel.repaint()
  }

  def drawRectangle (p1: Pixel, p2: Pixel): Unit = {
    val left = p1.x min p2.x
    val right = p1.x max p2.x
    val bottom = p1.y min p2.y
    val top = p1.y max p2.y
    graphics.drawRect(left, flipV(top), right-left, top-bottom)
    panel.repaint()
  }

  def fillRectangle (p1: Pixel, p2: Pixel): Unit = {
    val left = p1.x min p2.x
    val right = p1.x max p2.x
    val bottom = p1.y min p2.y
    val top = p1.y max p2.y
    graphics.fillRect(left, flipV(top), right-left, top-bottom)
    panel.repaint()
  }

  def drawPolyline (p: Seq[Pixel]): Unit = {
    val xs = p.map(_.x).toArray
    val ys = p.map((p: Pixel) ⇒ flipV(p.y)).toArray
	  graphics.drawPolyline(xs, ys, p.length)
    panel.repaint()
  }

  def fillPolygon(p: Seq[Pixel]): Unit = {
    val xs = p.map(_.x).toArray
    val ys = p.map((p: Pixel) ⇒ flipV(p.y)).toArray
	  graphics.fillPolygon(xs, ys, p.length)
    panel.repaint()
  }

  def drawArc (p0: Pixel, width: Int, height: Int, startAngle: Int, arcAngle: Int): Unit ={
    graphics.drawArc(p0.x-width/2, flipV(p0.y+height/2), width, height, startAngle, arcAngle)
    panel.repaint()
  }

  def fillArc (p0: Pixel, width: Int, height: Int, startAngle: Int, arcAngle: Int): Unit ={
    graphics.fillArc(p0.x-width/2, flipV(p0.y+height/2), width, height, startAngle, arcAngle)
    panel.repaint()
  }

  def drawText (p0: Pixel, text: String): Unit =
    if (text != null) {
      graphics.drawString(text, p0.x, flipV(p0.y))
      panel.repaint()
    }

  def close (): Unit = {
    val closing = new WindowEvent(frame, WindowEvent.WINDOW_CLOSING)
    frame.dispatchEvent(closing)
  }

  //bonus functionality, not required by super class
  private class FileMenu (val parent: Frame) extends ActionListener {

    private final val DefaultFilename = local(Key.DefaultFilename)

    private val formatNames = ImageIO.getWriterFormatNames.toSet.map((s: String) ⇒ s.toLowerCase())

    def actionPerformed (e: ActionEvent): Unit = {
      val dialog = new FileDialog(parent, local(Key.SaveImageAs) + s" $formatNames", FileDialog.SAVE)
      dialog.setVisible(true)
      Option(dialog.getFile) match {
        case None ⇒ log.warn(local(Key.NoFileToSave))
        case Some(name) ⇒ save(new File(dialog.getDirectory, name))
      }
    }

    private def save (file: File): Unit = {
      val format = file.getName.substring(file.getName.lastIndexOf('.')+1).toLowerCase()
      if (!formatNames.contains(format)) {
        log.error(local(Key.FormatNotAvailable) + s" $format")
        return
      }
    	val saveImage =
    	  if (image.getType==BufferedImage.TYPE_INT_RGB)
      	  image
      	else {
      	  val rgbImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
      	  val g = rgbImage.getGraphics
      	  g.drawImage(image, 0, 0, null)
      	  rgbImage
      	}
    	try {
    	  ImageIO.write(saveImage, format, file)
    	} catch {
    	  case t: IOException ⇒ log.error(local(Key.WritingFileException) + s" $file: $t")
    	}
    }

  }
}

object AwtScreenDriver {

  private val log = Logger(getClass) at Level.Error

  private val env = GraphicsEnvironment.getLocalGraphicsEnvironment
  private val bounds = if (env.isHeadlessInstance()) new Rectangle(0,0,0,0) else env.getMaximumWindowBounds
  val screenOrigin: Pixel = Pixel(bounds.x, bounds.y)
  val screenHeight: Int = bounds.height
  val screenWidth: Int = bounds.width

  private object Localizer {

    private[AwtScreenDriver] object Key extends Enumeration {
      type Key = Value
      val File, SaveAs, DefaultFilename, SaveImageAs, NoFileToSave, FormatNotAvailable,
          WritingFileException = Value
    }

    private val bundleBaseName = classOf[AwtScreenDriver].getName
    private val bundle = BundleConfig(bundleBaseName)
    if (bundle.isEmpty) log.error(s"empty or missing resource bundle: $bundleBaseName")

    import Key._

    private[AwtScreenDriver] def local (key: Key): String = {
      val default = key.toString()
      return bundle.get(key.toString()) match {
        case Some(str) ⇒ str
        case None ⇒ default
      }
    }

  }

  private def toColor (awt: AwtColor) =
    Color(awt.getRed, awt.getGreen, awt.getBlue, awt.getAlpha)

  private def toAwtColor (col: Color) =
    new AwtColor(col.red, col.green, col.blue, col.alpha)

}

