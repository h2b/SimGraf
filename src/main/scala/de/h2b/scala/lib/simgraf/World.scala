/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf

import scala.language.implicitConversions

import de.h2b.scala.lib.simgraf.shapes.{ Drawable, Fillable, Line }
import de.h2b.scala.lib.simgraf.layout.Cell
import de.h2b.scala.lib.simgraf.shapes.Rectangle

/**
 * Provides graphics in world coordinates.
 *
 * It will have a coordinate system specified by its lower-left and
 * upper-right corner. All operations ensure clipping to that area, so
 * that it is safe to use coordinates outside of it.
 *
 * Implementations of this abstract class must provide a `screen` object.
 *
 * @author h2b
 *
 * @constructor
 *
 * @param p1 lower left corner of the world coordinate system
 * @param p2 upper right corner of the world coordinate system
 *
 * @throws IllegalArgumentException if left bound of the world coordinate system
 * greater than or equal to right bound or lower bound of the world coordinate
 * system greater than or equal to upper bound
 */
abstract class World (val p1: Point, val p2: Point) {

  import World._

	require(p1.x<p2.x, s"left bound must be lower than right bound: ${p1.x}>=${p2.x}")
	require(p1.y<p2.y, s"lower bound must be lower than upper bound: ${p1.y}>=${p2.y}")

  def screen: Screen

  def width: Int = screen.width
  def height: Int = screen.height

  def activeColor: Color = screen.activeColor
  def activeColor_= (col: Color): Unit = {
    screen.activeColor = col
  }

  def activeFont: Font with FontMetrics = screen.activeFont
  def activeFont_= (font: Font with FontMetrics): Unit = {
    screen.activeFont = font
  }

  /**
   * Specifies preferred thickness of curves in pixels. (Meant as a suggestion,
   * not as a strict directive to be obeyed.)
   */
  var lineWidthHint: LineWidth = LineWidth(3)

  protected[simgraf] def scaleX = width/(p2.x-p1.x)
  protected[simgraf] def scaleY = height/(p2.y-p1.y)
  def screenX (x: Double): Int = ((x-p1.x)*scaleX).toInt
  def screenY (y: Double): Int = ((y-p1.y)*scaleY).toInt
  protected[simgraf] implicit def toPixel (p: Point): Pixel = Pixel(screenX(p.x), screenY(p.y))
  def worldX (x: Int): Double = p1.x+x/scaleX
  def worldY (y: Int): Double = p1.y+y/scaleY
  protected[simgraf] implicit def toPoint (p: Pixel): Point = Point(worldX(p.x), worldY(p.y))

  /**
   * Plots a point in a given color.
   *
   * @param p
   * @param col optional, defaults to active color
   */
  def plot (p: Point, col: Color = activeColor): Unit = screen.setPixel(p, col)

	/**
	 * Draws the specified shape within this world using the active color.
	 *
	 * @param d
	 */
  def draw (d: Drawable): Unit = d.draw(this)

	/**
	 * Fills the specified shape within this world using the active color.
	 *
	 * @param f
	 */
  def fill (f: Fillable): Unit = f.fill(this)

  /**
   * Sets the the whole world to a specified color.
   *
   * @param col specified color
   */
  def clear (col: Color): Unit = screen.clear(col)

  private var draw0 = Point(0,0)

  /**
   * Sets starting point for next {@code drawtTo}.
   *
   * No methods other than {@code moveTo} and {@code drawTo} have an effect on
   * this starting point.
   *
   * @param p
   */
  def moveTo (p: Point): Unit = {
    draw0 = p
  }

  /**
   * Draws a line in the active color from the active starting point to the
   * given endpoint and sets the latter as new starting point for the next
   * {@code drawTo} operation.
   *
   * No methods other than {@code moveTo} and {@code drawTo} have an effect on
   * the starting point.
   *
   * @param p
   */
  def drawTo (p: Point): Unit = {
	  draw(Line(draw0, p))
	  moveTo(p)
  }

}

object World {

  class LineWidth private (val value: Int)

  object LineWidth {
    def apply (value: Int): LineWidth = new LineWidth(value)
  }

  /**
   * @param p1 lower left corner of the world coordinate system
   * @param p2 upper right corner of the world coordinate system
	 * @param p0 location of the upper-left corner on the screen
	 * @param width horizontal dimension in pixels
	 * @param height vertical dimension in pixels
	 * @param title optional window title (will not reduce the drawing area,
	 * defaults to empty string)
	 * @return a new world on a screen based on a default screen driver
   *
   * @throws IllegalArgumentException if left bound of the world coordinate system
   * greater than or equal to right bound or lower bound of the world coordinate
   * system greater than or equal to upper bound
   */
  def apply (p1: Point, p2: Point)
      (p0: Pixel, width: Int, height: Int, title: String = ""): World =
    apply(Rectangle(p1, p2), Screen(p0, width, height, title))

  /**
   * @param frame bounding rectangle of the  world coordinate system
   * @param cell defines location, width and height of this world on the screen
	 * @param title window title (will not reduce the drawing area)
	 * @return a new world on a screen based on a default screen driver
   *
   * @throws IllegalArgumentException if left bound of the world coordinate system
   * greater than or equal to right bound or lower bound of the world coordinate
   * system greater than or equal to upper bound
   */
  def apply (frame: Rectangle) (cell: Cell, title: String): World =
    apply(frame, Screen(cell, title))

  /**
   * @param frame bounding rectangle of the  world coordinate system
   * @param cell defines location, width and height of this world on the screen
	 * @param title window title (will not reduce the drawing area)
	 * @return a new world on a screen based on a default screen driver with some
	 * input events enabled
   *
   * @throws IllegalArgumentException if left bound of the world coordinate system
   * greater than or equal to right bound or lower bound of the world coordinate
   * system greater than or equal to upper bound
   */
  def withEvents (frame: Rectangle) (cell: Cell, title: String): World =
    apply(frame, Screen.withEvents(cell, title))

  /**
   * @param frame bounding rectangle of the  world coordinate system
   * @param window screen on which the world is shown
	 * @return a new world on the given screen
   *
   * @throws IllegalArgumentException if left bound of the world coordinate system
   * greater than or equal to right bound or lower bound of the world coordinate
   * system greater than or equal to upper bound
   */
  def apply (frame: Rectangle, window: Screen): World = {
	  trait ScreenObj {
		  val screen: Screen = window
	  }
    new World(frame.p1, frame.p2) with ScreenObj
  }

}
