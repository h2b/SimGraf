/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf

/**
Shapes are used for the `World`'s `draw` and `fill` methods. A shape can
extend the `Shape.Drawable` or `Shape.Fillable` trait or both, which makes it
applicable to the according method.

The package comes with a variety of predefined shapes ranging from simple
figures like `Line`, `Rectangle` or `Circle` to higher-order ones like
`Function` or even `Grid` which draws a simple coordinate system into the
world.

Of course, you can define your own shapes. Just implement the `Shape.Drawable`
or `Shape.Fillable` trait(s).
 */
package object shapes
