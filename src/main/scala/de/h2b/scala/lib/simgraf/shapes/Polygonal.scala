/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.shapes

import de.h2b.scala.lib.simgraf.{ Point, World }

/**
 * A polygonal chain with vertices defined by the specified sequence.
 *
 * The chain is not closed by default. To achieve this, set the last vertex
 * equal to the first. Note that there is no need to close the sequence of vertices
 * when filling the polygonal area.
 *
 * @author h2b
 */
case class Polygonal (val p: Seq[Point]) extends Drawable with Fillable {

	def draw (w: World): Unit = w.screen.drawPolyline(p.map(w.toPixel(_)))

  def fill (w: World): Unit = w.screen.fillPolygon(p.map(w.toPixel(_)))

}
