/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf

/**
 * @since 1.4.0
 * @author h2b
 *
 * @constructor
 *
 * @param size
 * @param emphasis
 *
 * @note The attributes `size` and `emphasis` are implementation-dependent and
 * may have no effect at all.
 */
case class Font (size: Font.Size.Value = Font.Size.Normal,
    emphasis: Set[Font.Emphasis.Value] = Set.empty)

object Font {

  object Size extends Enumeration {
    type Size = Value
    val Tiny, Small, Normal, Large, Huge = Value
  }

  object Emphasis extends Enumeration {
    type Emphasis = Value
    val Bold, Italic = Value
  }

}

trait FontMetrics { font: Font ⇒

  case class Dimension (width: Int, height: Int)

  /**
   * @return the (width, height) of this character in pixels
   */
  def dimension (c: Char): Dimension

  /**
   * @return the (width, height) of this string in pixels
   */
  def dimension (s: String): Dimension

  /**
   * @return a new font metrics derived from this metrics using the specified
   * font
   */
  def derived (font: Font): Font with FontMetrics

}
