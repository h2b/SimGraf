/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf

import scala.collection.IterableLike

import de.h2b.scala.lib.math.linalg.VectorLike
import de.h2b.scala.lib.math.linalg.building.{ VectorBuilder, VectorCanBuildFrom }
import de.h2b.scala.lib.math.linalg.factory.IntVector

case class Pixel private (val x: Int, val y: Int) extends
    IntVector(1, Seq(x,y)) with IterableLike[Int, Pixel] with VectorLike[Int, Pixel] with
    PointStore[Int] {

  override protected[this] def newBuilder: VectorBuilder[Int,Pixel] = Pixel.newBuilder

}

object Pixel {

  def newBuilder: VectorBuilder[Int,Pixel] =
    new VectorBuilder[Int, Pixel] {
		  def result: Pixel = {
				  require(elems.length==2)
				  Pixel(elems(0), elems(1))
      }
  }

  implicit def canBuildFrom: VectorCanBuildFrom[Pixel, Int, Pixel] =
    new VectorCanBuildFrom[Pixel, Int, Pixel] {
	    def apply (): VectorBuilder[Int,Pixel] = newBuilder
      def apply (from: Pixel): VectorBuilder[Int,Pixel] = newBuilder
  }

	implicit class ScalarOps (s: Int) {
    def * (p: Pixel): Pixel = p * s
	}

}
