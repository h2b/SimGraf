/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.shapes

import de.h2b.scala.lib.simgraf.{ Pixel, World }

/**
 * A function as shape.
 *
 * When filling, this is done from the x axis (i.e. `y==0`).
 *
 * @constructor
 *
 * @param f
 *
 * @param enc enclosing for this function; defaults to the full world if not
 * explicitly or otherwise implicitly given
 *
 * @author h2b
 */
case class Function (val f: Double ⇒ Double) (implicit val enc: Enclosing)
    extends Drawable with Fillable {

	def draw (w: World): Unit = {
	  val (wl, wr, wb, wt) = enc.lrbt(w)
	  val (sl, sr, sb, st) = (w.screenX(wl), w.screenX(wr), w.screenY(wb), w.screenY(wt))
    for (x ← sl until sr) {
      val y = w.screenY(f(w.worldX(x)))
      val l2 = w.lineWidthHint.value/2
      for (h ← -l2 to +l2) {
        val yh = y+h
        if (sb<=yh && yh<=st) w.screen.setPixel(Pixel(x, yh), w.activeColor)
      }
    }
	}

  def fill (w: World): Unit = {
	  val (wl, wr, wb, wt) = enc.lrbt(w)
	  val (sl, sr, sb, st) = (w.screenX(wl), w.screenX(wr), w.screenY(wb), w.screenY(wt))
	  def clipY (y: Int): Int =
	    if (y<sb) sb
	    else if (st<y) st
	    else y
	  val s0 = clipY(w.screenY(0))
    for (x ← sl until sr)
      w.screen.drawLine(Pixel(x,s0), Pixel(x, clipY(w.screenY(f(w.worldX(x))))))
  }

}
