/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.event

import de.h2b.scala.lib.simgraf.Pixel

/**
 * Sealed trait of input events.
 *
 * @since 1.2.0
 * @author h2b
 */
sealed trait Event

case class KeyEvent (char: Char) extends Event

/**
 * @constructor
 *
 * @param position the position of this mouse event in coordinates of the
 * related screen
 */
case class MouseEvent (button: Button, modifiers: Set[Modifier], clickCount: Int,
    position: Pixel) extends Event

/**
 * @constructor
 *
 * @param position0 the start position of this drag event in coordinates
 * of the related screen
 * @param position1 the end position of this drag event in coordinates
 * of the related screen
 */
case class DragEvent (button: Button, modifiers: Set[Modifier],
    position0: Pixel, position1: Pixel) extends Event

/**
 * @constructor
 *
 * @param position the position of this wheel event in coordinates of the
 * related screen
 * @param rotation the number of "clicks" the mouse wheel was rotated
 * (negative when up/away from the user, and positive when down/towards
 * the user)
 */
case class WheelEvent (button: Button, modifiers: Set[Modifier], clickCount: Int,
    position: Pixel, rotation: Int) extends Event
