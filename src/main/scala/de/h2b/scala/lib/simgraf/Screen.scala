/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf

import scala.concurrent.Future

import de.h2b.scala.lib.simgraf.driver.{ AwtScreenDriver, ScreenDriver }
import de.h2b.scala.lib.simgraf.layout.Cell
import de.h2b.scala.lib.simgraf.driver.AwtEventDriver

/**
 * Provides screen (pixel) graphics.
 *
 * The drawing area will have the specified width and height and will be
 * positioned at the given location (if possible).
 *
 * It will have a coordinate system with the origin `(0, 0)` in the lower-left
 * corner and the upper-right corner at `(width-1, height-1)`. All operations
 * ensure clipping to that area, so that it is safe to use coordinates outside
 * of it.
 *
 * Implementation note: This abstract class does not add abstract members other
 * than it inherits from ` ScreenDriver`.
 *
 * @author h2b
 *
 * @constructor
 *
 * @param p0 location of the upper-left corner on the screen
 * @param width horizontal dimension
 * @param height vertical dimension
 * @param title optional window title (will not reduce the drawing area,
 * defaults to empty string)
 */
abstract class Screen (override val p0: Pixel, override val width: Int,
    override val height: Int, override val title: String = "") extends
    ScreenDriver(p0, width, height, title) {

  /**
   * Draws a square with centre at `p0` and the specified side length `l`.
   *
   * @param p0 centre of the square
   * @param l side length of the square
   */
  def drawSquare (p0: Pixel, l: Int): Unit =
	  drawRectangle(Pixel(p0.x-l/2, p0.y-l/2), Pixel(p0.x+l/2, p0.y+l/2))

  /**
   * Fills a square with centre at `p0` and the specified side length `l`.
   *
   * @param p0 centre of the square
   * @param l side length of the square
   */
  def fillSquare (p0: Pixel, l: Int): Unit =
	  fillRectangle(Pixel(p0.x-l/2, p0.y-l/2), Pixel(p0.x+l/2, p0.y+l/2))

  /**
   * Draws a cross (+) with centre at `p0` and the specified line length `l`.
   *
   * @param p0 centre of the cross
   * @param l length of the lines of the cross
   */
  def drawCross (p0: Pixel, l: Int): Unit = {
	  drawLine(Pixel(p0.x-l/2, p0.y), Pixel(p0.x+l/2, p0.y))
	  drawLine(Pixel(p0.x, p0.y-l/2), Pixel(p0.x, p0.y+l/2))
  }

  /**
   * Draws an ellipse within a virtual rectangle.
   *
   * @param p0 centre of the ellipse
   * @param width of the enclosing rectangle
   * @param height of the enclosing rectangle
   */
  def drawEllipse (p0: Pixel, width: Int, height: Int): Unit =
    drawArc(p0, width, height, 0, 360)

  /**
   * Fills an ellipse within a virtual rectangle.
   *
   * @param p0 centre of the ellipse
   * @param width of the enclosing rectangle
   * @param height of the enclosing rectangle
   */
  def fillEllipse (p0: Pixel, width: Int, height: Int): Unit =
    fillArc(p0, width, height, 0, 360)

  /**
   * Draws a circle.
   *
   * @param p0 centre of the circle
   * @param r radius of the circle
   */
  def drawCircle (p0: Pixel, r: Int): Unit =
    drawEllipse(p0, 2*r, 2*r)

  /**
   * Fills a circle.
   *
   * @param p0 centre of the circle
   * @param r radius of the circle
   */
  def fillCircle (p0: Pixel, r: Int): Unit =
    fillEllipse(p0, 2*r, 2*r)

  /**
   * Sets all pixels in a rectangle with diagonal opposite corners at `p1` and `p2`
   * to a computed color.
   *
   * @param p1
   * @param p2
   * @param col computing function that maps pixel coordinates to colors
   */
  def setPixels (p1: Pixel, p2: Pixel) (col: Pixel ⇒ Color): Unit = {
    val left = p1.x min p2.x
    val right = p1.x max p2.x
    val bottom = p1.y min p2.y
    val top = p1.y max p2.y
	  for {
	    x ← left to right
	    y ← bottom to top
	  } {
	    val p = Pixel(x,y)
	    setPixel(p, col(p))
	  }
  }

  /**
   * Sets all pixels of the screen to a computed color.
   *
   * @param col computing function that maps pixel coordinates to colors
   */
  def setPixels (col: Pixel ⇒ Color): Unit =
    setPixels(Pixel(0,0), Pixel(width-1,height-1))(col)

  /**
   * Sets all pixels in a rectangle with diagonal opposite corners at `p1` and `p2`
   * to a computed color using parallel operations.
   *
   * The returned value can be used if further operations need to be informed
   * when this operation is done (e.g., by setting a callback). The future is
   * completed when either all pixels are set or some exception occurs. Note
   * that in case of an exception some pixel settings might still continue
   * afterwards.
   *
   * @param p1
   * @param p2
   * @param col computing function that maps pixel coordinates to colors
   * @return a future with no specific value indicating the completion of this
   * operation
   * @since 1.1.0
   */
  def setPixelsParallel (p1: Pixel, p2: Pixel) (col: Pixel ⇒ Color): Future[Unit] = {
    val left = p1.x min p2.x
    val right = p1.x max p2.x
    val bottom = p1.y min p2.y
    val top = p1.y max p2.y
    val futures = for (y ← bottom to top) yield Future {
      for (x ← left to right) {
  	    val p = Pixel(x,y)
  	    setPixel(p, col(p))
      }
    }
    Future.sequence(futures) map { _ ⇒ () }
  }

  /**
   * Sets all pixels of the screen to a computed color using parallel
   * operations.
   *
   * The returned value can be used if further operations need to be informed
   * when this operation is done (e.g., by setting a callback). The future is
   * completed when either all pixels are set or some exception occurs. Note
   * that in case of an exception some pixel settings might still continue
   * afterwards.
   *
   * @param col computing function that maps pixel coordinates to colors
   * @return a future with no specific value indicating the completion of this
   * operation
   * @since 1.1.0
   */
  def setPixelsParallel (col: Pixel ⇒ Color): Future[Unit] =
    setPixelsParallel(Pixel(0,0), Pixel(width-1,height-1))(col)

  /**
   * Sets all pixels in a rectangle with diagonal opposite corners at `p1` and `p2`
   * to a specified color.
   *
   * @param p1
   * @param p2
   * @param col specified color
   */
  def clear (p1: Pixel, p2: Pixel, col: Color): Unit = {
    val previousColor = activeColor
    activeColor = col
    fillRectangle(p1, p2)
    activeColor = previousColor
  }

  /**
   * Sets all pixels of the screen to a specified color.
   *
   * @param col specified color
   */
  def clear (col: Color): Unit =
    clear(Pixel(0,0), Pixel(width-1,height-1), col)

  /**
   * Draws a grid in a rectangle with diagonal opposite corners at `p1` and `p2`.
   *
   * @param p1
   * @param p2
   * @param dx distance of vertical lines
   * @param dy distance of horizontal lines
   */
  def drawGrid (p1: Pixel, p2: Pixel, dx: Int, dy: Int): Unit = {
    val left = p1.x min p2.x
    val right = p1.x max p2.x
    val bottom = p1.y min p2.y
    val top = p1.y max p2.y
	  for (x ← left until right by dx) {
		  drawText(Pixel(x, bottom), x.toString())
		  drawLine(Pixel(x, bottom), Pixel(x, top))
	  }
	  for (y ← bottom until top by dy) {
		  drawText(Pixel(left, y), y.toString())
		  drawLine(Pixel(left, y), Pixel(right, y))
	  }
  }

  /**
   * Draws a grid on the whole screen.
   *
   * @param dx distance of vertical lines
   * @param dy distance of horizontal lines
   */
  def drawGrid (dx: Int, dy: Int): Unit =
    drawGrid(Pixel(0,0), Pixel(width-1,height-1), dx, dy)

  private var draw0 = Pixel(0,0)

  /**
   * Sets starting point for next {@code drawtTo}.
   *
   * No methods other than {@code moveTo} and {@code drawTo} have an effect on
   * this starting point.
   *
   * @param p
   */
  def moveTo (p: Pixel) = {
    draw0 = p
  }

  /**
   * Draws a line in the active color from the active starting point to the
   * given endpoint and sets the latter as new starting point for the next
   * {@code drawTo}.
   *
   * No methods other than {@code moveTo} and {@code drawTo} have an effect on
   * the starting point.
   *
   * @param p
   */
  def drawTo (p: Pixel) = {
	  drawLine(draw0, p)
	  moveTo(p)
  }

}

object Screen {

	/** screen origin **/
  val origin: Pixel = ScreenDriver.screenOrigin
	/** available screen height in pixels */
  val height: Int = ScreenDriver.screenHeight
	/** available screen width in pixels */
  val width: Int = ScreenDriver.screenWidth

	/**
	 * @param p0 location of the upper-left corner on the screen
	 * @param width horizontal dimension
	 * @param height vertical dimension
	 * @param title optional window title (will not reduce the drawing area,
	 * defaults to empty string)
	 * @return a new screen based on a default driver
	 */
  def apply (p0: Pixel, width: Int, height: Int, title: String = ""): Screen =
    awt(p0, width, height, title)

  /**
   * @param cell defines location, width and height of this screen
   * @param title window title (will not reduce the drawing area)
   * @return a new screen based on a default driver
   */
  def apply (cell: Cell, title: String): Screen =
    apply(cell.origin, cell.width, cell.height, title)

  /**
   * @param cell defines location, width and height of this screen
   * @param title window title (will not reduce the drawing area)
   * @return a new screen based on a default driver with some input events enabled
   */
  def withEvents (cell: Cell, title: String): Screen =
    awtWithEvents(cell, title)

  /**
   * @param p0 location of the upper-left corner on the screen
   * @param width horizontal dimension
   * @param height vertical dimension
   * @param title optional window title (will not reduce the drawing area,
   * defaults to empty string)
   * @return a new screen based on AWT driver
   */
  def awt (p0: Pixel, width: Int, height: Int, title: String = ""): Screen =
    new Screen(p0, width, height, title) with AwtScreenDriver

  /**
   * @param cell defines location, width and height of this screen
   * @param title window title (will not reduce the drawing area)
   * @return a new screen based on AWT driver
   */
  def awt (cell: Cell, title: String): Screen =
    awt(cell.origin, cell.width, cell.height, title)

  /**
   * @param cell defines location, width and height of this screen
   * @param title window title (will not reduce the drawing area)
   * @return a new screen based on AWT driver with key and mouse events enabled
   */
  def awtWithEvents (cell: Cell, title: String): Screen =
    new Screen(cell.origin, cell.width, cell.height, title) with AwtEventDriver

}

