/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.shapes

import de.h2b.scala.lib.simgraf.{ Point, World }
import de.h2b.scala.lib.simgraf.shapes.Grid.Label

/**
 * A grid with labels.
 *
 * @constructor
 *
 * @param dx distance of vertical lines
 * @param dy distance of horizontal lines
 * @param label optional label formatting
 *
 * @param enc enclosing for this grid; defaults to the full world if not
 * explicitly or otherwise implicitly given
 *
 * @author h2b
 */
case class Grid (val dx: Double, val dy: Double, val label: Label = Label.Default)
    (implicit val enc: Enclosing) extends Drawable {

	def draw (w: World): Unit = {
	  val (left, right, bottom, top) = enc.lrbt(w)
	  for (x ← left until right by dx) {
		  Line(Point(x, bottom), Point(x, top)).draw(w)
		  if (label.on) Text(Point(x, bottom), label.formatX.format(x)).draw(w)
	  }
	  for (y ← bottom until top by dy) {
		  Line(Point(left, y), Point(right, y)).draw(w)
		  if (label.on) Text(Point(left, y), label.formatY.format(y)).draw(w)
	  }
  }

}

object Grid {

	/**
	 * Defines labels for the grid.
	 *
	 * @author h2b
	 *
	 * @param formatX format string for x labels according to `StringOps.format`
	 * @param formatY format string for y labels according to `StringOps.format`
	 * @param on denotes if labels should be shown at all
	 */
	class Label private (val formatX: String, val formatY: String, val on: Boolean)

	object Label {
		final val Default = Label("%1.1f")
		final val None = Label()
	  /** Returns a new label with x and y formats. */
		def apply (formatX: String, formatY: String): Label = new Label(formatX, formatY, true)
	  /** Returns a new label with one format for x and y. */
		def apply (formatXY: String): Label = new Label(formatXY, formatXY, true)
		/** Returns a new label that is switched off. */
		def apply (): Label = new Label("", "", false)
	}

}
