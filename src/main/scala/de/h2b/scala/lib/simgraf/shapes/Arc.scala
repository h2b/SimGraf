/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.shapes

import de.h2b.scala.lib.simgraf.{ Point, World }

/**
 * An arc within a virtual rectangle.
 *
 * Angles are given in radian measure, where 0 is the direction of the x axis and
 * positive values count counter-clockwise while negative values count
 * clockwise.
 *
 * @author h2b
 *
 * @constructor
 *
 * @param p0 centre of the arc
 * @param width of the enclosing virtual rectangle
 * @param height of the enclosing virtual rectangle
 * @param startAngle start in radians
 * @param arcAngle extent in radians
 *
 * @example Arc(Point(10,10), 5, 5, 0, 2*Math.Pi) creates a circle of diameter 5
 * around the centre (10,10)
 */
case class Arc (val p0: Point, val width: Double, val height: Double,
    val startAngle: Double, val arcAngle: Double) extends Drawable with Fillable {

	def draw (w: World): Unit =
    w.screen.drawArc(w.toPixel(p0), (width*w.scaleX).toInt, (height*w.scaleY).toInt,
        startAngle.toDegrees.toInt, arcAngle.toDegrees.toInt)

  def fill (w: World): Unit =
    w.screen.fillArc(w.toPixel(p0), (width*w.scaleX).toInt, (height*w.scaleY).toInt,
        startAngle.toDegrees.toInt, arcAngle.toDegrees.toInt)

}
