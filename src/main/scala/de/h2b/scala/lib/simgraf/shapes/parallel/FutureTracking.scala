/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.shapes.parallel

import scala.concurrent.Future

/**
 * Provides some measures to check for completion of futures.
 *
 * @since 1.1.0
 * @author h2b
 */
trait FutureTracking {

  @volatile
  protected var future = Future.successful(()) //Future.unit apparently requires 2.12

  /**
   * Indicates whether some concurrent operation is done.
   *
   * The returned value can be used if further operations need to be informed
   * when some operation is completed (e.g., by setting a callback).
   *
   * @note When calling one or more methods of this instance concurrently, the
   * result is not deterministic: you get the value that the last currently
   * active operation left. In this case and also generally when an exception
   * is returned, further operations might still going on.
   *
   * @return a future with no specific value indicating the completion of some
   * operation
   */
  def currentFuture: Future[Unit] = future

}
