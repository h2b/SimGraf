/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf

import scala.collection.IterableLike

import de.h2b.scala.lib.math.linalg.VectorLike
import de.h2b.scala.lib.math.linalg.building.{ VectorBuilder, VectorCanBuildFrom }
import de.h2b.scala.lib.math.linalg.factory.DoubleVector

case class Point private (val x: Double, val y: Double) extends
    DoubleVector(1, Seq(x,y)) with IterableLike[Double, Point] with VectorLike[Double, Point] with
    PointStore[Double] {

  override protected[this] def newBuilder: VectorBuilder[Double,Point] = Point.newBuilder

}

object Point {

  def newBuilder: VectorBuilder[Double,Point] =
    new VectorBuilder[Double, Point] {
		  def result: Point = {
				  require(elems.length==2)
				  Point(elems(0), elems(1))
      }
  }

  implicit def canBuildFrom: VectorCanBuildFrom[Point, Double, Point] =
    new VectorCanBuildFrom[Point, Double, Point] {
	    def apply (): VectorBuilder[Double,Point] = newBuilder
      def apply (from: Point): VectorBuilder[Double,Point] = newBuilder
  }

	implicit class ScalarOps (s: Double) {
    def * (p: Point): Point = p * s
	}

}

