/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib

import scala.concurrent.ExecutionContext

/**
This is a Scala library of graphics utilities. It is focused on simple drawing
of shapes and functions without the need of instructing the graphics system by
a dozen or so settings before a simple picture is shown. It is not meant to
program graphical user interfaces with buttons, menus and so on.

=The World=

A `World` provides graphics in world coordinates. For instance,

{{{
    val w = World(p1, p2)(p0, w, h, title)
}}}

defines a world that has a coordinate system with a `Point` `p1` in the
lower-left corner and the upper-right corner at `p2`. All operations ensure
clipping to that area, so that it is safe to use coordinates outside of it.

The second parameter group defines the location on the screen: `p0` denotes
the upper left `Pixel`, `w` and `h` the width and the height of the window in
pixels and `title` gives a window title (that will not reduce the drawing
area).

A `Point` defines a location in the world coordinate system using two doubles,
while a `Pixel` denotes a location on the screen by means of two integers.

Note that the y axis of the world coordinate system is directed from bottom to
top while at the screen level it is vice versa.

So, for example

{{{
    val w = World(Point(-100,-100), Point(100,100))(Pixel(0,0), 200, 200, "Circle")
}}}

defines a world with x and y axis both ranging from -100 to +100 shown in a
window of size 200x200 pixels at the upper left corner of the screen titled
"Circle".

Once you have a world, you can execute several methods on it: `plot` a point
or `clear` the world to a specified color, use `moveTo` or `drawTo` for
plotter-like operations and -- at the highest abstraction -- `draw` or `fill`
shapes of type `Drawable` or `Fillable`, respectively.

Each world maintains an `activeColor` which can be set and is used for most
drawings and fillings until it is changed (except for those that use their own
color).

To fill a circle of color `Magenta` and radius 75.0 around the origin of our
world `w` on a white background, we write:

{{{
    w.clear(Color.White)
    w.activeColor = Color.Magenta
    w.fill(Circle(Point(0,0), 75.0))
}}}

That's it: with these three lines of code and the definition of `w` above you
get a graphic on the screen.

=The Screen=

A `Screen` provides direct pixel graphics. It is the back end of `World`.

It can be used on its own if no world coordinate system is needed and bare
screen-pixel coordinates shall be applied instead. Though, there are no fancy
general shape-oriented `draw` and `fill` operations as `World` has to offer,
but only some primitives like `setPixel`, `drawLine`, `drawSquare`,
`fillSquare`, `moveTo` or `drawTo`.
 */
package object simgraf {

  private[simgraf] implicit lazy val packageExecutionContext: ExecutionContext = ExecutionContext.global

}
