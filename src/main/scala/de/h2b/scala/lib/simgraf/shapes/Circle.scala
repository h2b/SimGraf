/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.shapes

import de.h2b.scala.lib.simgraf.{ Point, World }

import scala.math._

/**
 * A circle.
 *
 * @author h2b
 *
 * @constructor
 *
 * @param p0 centre of the circle
 * @param r radius of the circle
 */
case class Circle (val p0: Point, val r: Double) extends Drawable with Fillable {

  private val ell = Ellipse(p0, 2*r, 2*r)

	def draw (w: World): Unit = ell.draw(w)

  def fill (w: World): Unit = ell.fill(w)

}
