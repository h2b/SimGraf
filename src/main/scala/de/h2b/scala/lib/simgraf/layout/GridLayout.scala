/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.layout

import de.h2b.scala.lib.simgraf.{ Pixel, Screen }

object Floating extends Enumeration {
  type Floating = Value
  val Horizontal, Vertical = Value
}

import Floating._

/**
 * A layout of cells organized into rows and columns as this:
 *
 * <pre>
 * ----------------------------
 * | cell | cell | cell | ... |
 * ----------------------------
 * | cell | cell | cell | ... |
 * ----------------------------
 * | cell | cell | cell | ... |
 * ----------------------------
 * | ...  | ...  | ...  | ... |
 * ----------------------------
 * </pre>
 *
 * The horizontal axis is oriented from left to right, while the vertical axis
 * is oriented from top to bottom.
 *
 * @since 1.1.0
 * @author h2b
 */
class GridLayout private (val rows: Int, val cols: Int, val cell: Cell,
    val floating: Floating) extends Cell(cols*cell.width, rows*cell.height, cell.origin) with
    Iterable[Cell] {

  /**
   * A copy of this grid layout at a new origin.
   */
  override def at (origin: Pixel): GridLayout =
    new GridLayout(rows, cols, cell at origin, floating)

  def iterator = floating match {
    case Horizontal ⇒ floatingIterator(rows, cols, (i, j) ⇒ (i, j))
    case Vertical ⇒ floatingIterator(cols, rows, (i, j) ⇒ (j, i))
  }

  private def floatingIterator (dim1: Int, dim2: Int, index: (Int, Int) ⇒ (Int, Int)) =
    new Iterator[Cell] {
      private var i, j = 0
      def hasNext = i < dim1
      def next = {
  		  if (!hasNext) throw new NoSuchElementException("iterator overflow")
  		  val (k, l) = index(i, j)
  		  val result = cell at (cell.origin + Pixel(l*cell.width, k*cell.height))
  		  j += 1
  		  if (j == dim2) { i += 1; j = 0 }
  		  result
      }
  }

  override def toString = s"GridLayout($rows, $cols, $cell, $floating)"

}

object GridLayout {

  /** Floating from left to right. */
  final val DefaultFloating = Floating.Horizontal

  /**
   * Constructs a new grid layout of the specified number of rows and columns
   * using cells of dimensions of the given sample cell. Also, the sample
   * cell's origin is the origin of the resulting layout.
   *
   * @param floating instructs the iterator to visit the cells either in
   * horizontal (first left to right, then top to bottom) or vertical (vice
   * versa) order (defaults to horizontal)
   */
  def apply (rows: Int, cols: Int, cell: Cell, floating: Floating = DefaultFloating): GridLayout =
    new GridLayout(rows, cols, cell, floating)

  /**
   * Constructs a new grid layout of the specified number of rows and columns
   * within the given cell.
   *
   * @param floating instructs the iterator to visit the cells either in
   * horizontal (first left to right, then top to bottom) or vertical (vice
   * versa) order (defaults to horizontal)
   *
   * @throws ArithmeticException if `rows` or `cols` is zero
   */
  def withinCell (cell: Cell, rows: Int, cols: Int, floating: Floating = DefaultFloating): GridLayout = {
    val inner = Cell(cell.width/cols, cell.height/rows, cell.origin)
    new GridLayout(rows, cols, inner, floating)
  }

  /**
   * Constructs a new grid layout of the specified number of rows and columns
   * on the whole screen.
   *
   * @param floating instructs the iterator to visit the cells either in
   * horizontal (first left to right, then top to bottom) or vertical (vice
   * versa) order (defaults to horizontal)
   *
   * @throws ArithmeticException if `rows` or `cols` is zero
   */
  def onScreen (rows: Int, cols: Int, floating: Floating = DefaultFloating): GridLayout = {
    withinCell(Cell(Screen.width, Screen.height), rows, cols, floating)
  }

}
