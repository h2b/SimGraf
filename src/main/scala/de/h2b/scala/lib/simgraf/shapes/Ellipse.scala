/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.shapes

import de.h2b.scala.lib.simgraf.{ Point, World }

import scala.math._

/**
 * An ellipse within a virtual rectangle.
 *
 * @author h2b
 *
 * @constructor
 *
 * @param p0 centre of the ellipse
 * @param width of the enclosing virtual rectangle
 * @param height of the enclosing virtual rectangle
 */
case class Ellipse (val p0: Point, val width: Double, val height: Double) extends Drawable with Fillable {

  private val arc = Arc(p0, width, height, 0, 2*Pi)

	def draw (w: World): Unit = arc.draw(w)

  def fill (w: World): Unit = arc.fill(w)

}
