/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.event

import akka.actor.Actor
import de.h2b.scala.lib.simgraf.Screen
import de.h2b.scala.lib.util.{ Level, Logger }

/**
 * Provides an actor publishing `Event`s to the package object's global event
 * stream.
 *
 * @since 1.2.0
 * @author h2b
 *
 * @constructor
 *
 * @param screen optional classifier
 */
class Publisher (val screen: Option[Screen] = None) extends Actor {

  private val log = Logger(getClass) at Level.Warn

  def receive = {
    case e: Event ⇒
      log.debug(s"event received: $e")
      stream.publish(e → screen)
    case unknown ⇒
      log.warn(s"unknown message received: $unknown")
  }

}
