/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.event

import akka.actor.{ Actor, ActorRef, Props }
import de.h2b.scala.lib.simgraf.{ Screen, World }
import de.h2b.scala.lib.util.{ Level, Logger }

/**
 * Provides an actor subscribing to `Event`s received from the package object's
 * global event stream for each of the specified screens.
 *
 * @since 1.2.0
 * @author h2b
 */
class Subscriber private (val screens: Set[Screen])
    (val handler: PartialFunction[Event, Unit]) extends Actor {

  override def preStart =
    if (screens.isEmpty) stream.subscribe(self, None)
    else for (screen ← screens) stream.subscribe(self, Some(screen))

  private val log = Logger(getClass) at Level.Warn

  def receive = {
    case e: Event ⇒
      log.debug(s"event received: $e")
      handler(e)
    case unknown ⇒
      log.warn(s"unknown message received: $unknown")
  }

}

object Subscriber {

	/**
	 * Subscribes to events not associated with a screen.
	 *
	 * @note Currently, no such events are published by this package.
	 *
	 * @param handler partial function handling received events
	 * @return a reference to a new subscriber
	 */
	def ref (handler: PartialFunction[Event, Unit]): ActorRef =
	  system.actorOf(Props(new Subscriber(Set.empty)(handler)))

	/**
	 * Subscribes to events of specified screens.
	 *
	 * @param screen screen to be listened for events
	 * @param moreScreens additional screens to be listened for events (may be
	 * empty)
	 * @param handler partial function handling received events
	 * @return a reference to a new subscriber
	 *
	 * @since 1.3.0
	 */
	def to (screen: Screen, moreScreens: Screen*)
	    (handler: PartialFunction[Event, Unit]): ActorRef =
	  system.actorOf(Props(new Subscriber(moreScreens.toSet + screen)(handler)))

	/**
	 * Subscribes to events of specified worlds.
	 *
	 * @param world world to be listened for events
	 * @param moreWorlds additional worlds to be listened for events (may be
	 * empty)
	 * @param handler partial function handling received events
	 * @return a reference to a new subscriber
	 *
	 * @since 1.3.0
	 */
	def to (world: World, moreWorlds: World*)
	    (handler: PartialFunction[Event, Unit]): ActorRef =
	  to(world.screen, (moreWorlds map {_.screen}): _*)(handler)

}
