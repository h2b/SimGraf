/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.driver

import java.awt.Color
import java.awt.event.{ KeyEvent ⇒ AwtKeyEvent, KeyListener ⇒ AwtKeyListener,
  MouseEvent ⇒ AwtMouseEvent, MouseListener ⇒ AwtMouseListener,
  MouseMotionListener ⇒ AwtMouseMotionListener,
  MouseWheelEvent ⇒ AwtMouseWheelEvent,
  MouseWheelListener ⇒ AwtMouseWheelListener }

import akka.actor.Props
import de.h2b.scala.lib.simgraf.{ Pixel, Screen }
import de.h2b.scala.lib.simgraf.event.{ AltGraphModifier, AltModifier, Button,
  CtrlModifier, DragEvent, KeyEvent, LeftButton, MetaModifier, MiddleButton,
  Modifier, MouseEvent, NoButton, OtherButton, Publisher, RightButton,
  ShiftModifier, WheelEvent, system }
import javax.swing.SwingUtilities

/**
 * Provides key and mouse listeners publishing key and mouse events to the
 * event-package publisher.
 *
 * @note As of version 1.3.0, this trait can only be mixed into a class that is
 * a subtype of `Screen`, which is required now and was the intention from the
 * beginning.
 *
 * @since 1.2.0
 * @author h2b
 */
trait AwtEventDriver extends AwtScreenDriver { self: Screen ⇒

  import AwtEventDriver._

  private val publisher = system.actorOf(Props(new Publisher(Some(self))))

  panel.addKeyListener(KeyListener)
  panel.addMouseListener(MouseListener)
  panel.addMouseMotionListener(MouseListener)
  panel.addMouseWheelListener(WheelListener)
  panel.requestFocusInWindow() //required for key events to be triggered

  private object KeyListener extends AwtKeyListener {

    def keyPressed (e: AwtKeyEvent): Unit = {
  	  //unused in this implementation
    }

    def keyReleased (e: AwtKeyEvent): Unit = {
  	  //unused in this implementation
    }

    def keyTyped (e: AwtKeyEvent): Unit = {
      publisher ! keyEvent(e)
    }

  }

  private object MouseListener extends AwtMouseListener with AwtMouseMotionListener {

    //AwtMouseListener events

    def mouseClicked (e: AwtMouseEvent): Unit = {
      panel.requestFocusInWindow()
      graphics.setXORMode(dragColor)
      for (m ← dragMarker) drawRectangle(m._1, m._2)
      dragMarker = None
      graphics.setPaintMode()
  		publisher ! mouseEvent(e)
    }

    def mouseEntered (e: AwtMouseEvent): Unit = {
  	  //unused in this implementation
    }

    def mouseExited (e: AwtMouseEvent): Unit = {
  	  //unused in this implementation
    }

    def mousePressed (e: AwtMouseEvent): Unit = {
      drag0 = position(e)
    }

    def mouseReleased (e: AwtMouseEvent): Unit = {
      if (dragging) {
        publisher ! dragEvent(e)
        dragging = false
      }
    }

    //AwtMouseMotionListener events

    private var dragging = false
    private var drag0 = Pixel(0, 0)
    private var dragMarker: Option[(Pixel, Pixel)] = None
    private val dragColor = Color.BLACK

    def mouseDragged (e: AwtMouseEvent): Unit = {
      val pos = position(e)
      graphics.setXORMode(dragColor)
      for (m ← dragMarker) drawRectangle(m._1, m._2)
      drawRectangle(drag0, pos)
      dragMarker = Some(drag0, pos)
      graphics.setPaintMode()
      dragging = true
    }

    def mouseMoved (e: AwtMouseEvent): Unit = {
  	  //unused in this implementation
    }

    private def dragEvent(e: AwtMouseEvent) =
			DragEvent(button(e), modifiers(e), drag0, position(e))

  }

  private object WheelListener extends AwtMouseWheelListener {

    def mouseWheelMoved (e: AwtMouseWheelEvent): Unit = {
      publisher ! wheelEvent(e)
    }

  }

  private def mouseEvent (e: AwtMouseEvent) =
    MouseEvent(button(e), modifiers(e), e.getClickCount, position(e))

  private def position (e: AwtMouseEvent) = Pixel(e.getX, flipV(e.getY))

  private def wheelEvent (e: AwtMouseWheelEvent) =
    WheelEvent(button(e), modifiers(e), e.getClickCount, position(e),
        e.getWheelRotation)

}

object AwtEventDriver {

  private def keyEvent (e: AwtKeyEvent) =
    KeyEvent(e.getKeyChar)

  private def button (e: AwtMouseEvent): Button =
    if (SwingUtilities.isLeftMouseButton(e)) LeftButton
    else if (SwingUtilities.isRightMouseButton(e)) RightButton
    else if (SwingUtilities.isMiddleMouseButton(e)) MiddleButton
    else e.getButton match {
      case AwtMouseEvent.NOBUTTON ⇒ NoButton
      case other ⇒ OtherButton(other)
    }

  private def modifiers (e: AwtMouseEvent) = {
    var result = Set.empty[Modifier]
    if (e.isShiftDown()) result += ShiftModifier
    if (e.isControlDown()) result += CtrlModifier
    if (e.isAltDown()) result += AltModifier
    if (e.isAltGraphDown()) result += AltGraphModifier
    if (e.isMetaDown()) result += MetaModifier
    result
  }

}
