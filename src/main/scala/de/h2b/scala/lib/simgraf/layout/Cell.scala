/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.layout

import de.h2b.scala.lib.simgraf.Pixel

/**
 * A rectangular area.
 *
 * The horizontal axis is oriented from left to right, while the vertical axis
 * is oriented from top to bottom.
 *
 * @since 1.1.0
 * @author h2b
 */
class Cell protected (val width: Int, val height: Int, val origin: Pixel) extends Equals {

  import Cell.HorizontalOrientation._
  import Cell.VerticalOrientation._

  /**
   * A copy of this cell at a new origin.
   */
  def at (origin: Pixel): Cell = new Cell(width, height, origin)

  /**
   * Returns a new cell with specified dimensions and orientation within this cell.
   *
   * @note If the cell to be fitted in does not fit into this cell the resulting
   * cell's origin might be negative.
   *
   * @param w the width of of the cell to be fitted in
   * @param h the height of of the cell to be fitted in
   * @param o the orientation of of the cell to be fitted in within this cell
   * (defaults to centring both dimensions)
   * @return the resulting new cell
   */
  def fit (w: Int, h: Int, o: Cell.Orientation = Cell.DefaultOrientation): Cell = {
    val x = o.horizontal match {
      case Left ⇒ 0
      case HCenter ⇒ (width -w)/2
      case Right ⇒ width-w
    }
    val y = o.vertical match {
      case Top ⇒ 0
      case VCenter ⇒ (height-h)/2
      case Bottom ⇒ height-h
    }
    new Cell(w, h, origin + Pixel(x, y))
  }

  override def toString = s"Cell($width, $height, $origin)"

  def canEqual (other: Any) = {
    other.isInstanceOf[Cell]
  }

  override def equals(other: Any) = {
    other match {
      case that: Cell =>
        that.canEqual(this) && width == that.width && height == that.height && origin == that.origin
      case _ =>
        false
    }
  }

  override def hashCode() = {
    val prime = 41
    prime * (prime * (prime + width.hashCode) + height.hashCode) + origin.hashCode
  }

}

object Cell {

	object HorizontalOrientation extends Enumeration {
	  type HorizontalOrientation = Value
	  val Left, HCenter, Right = Value
	}

	import HorizontalOrientation._

	object VerticalOrientation extends Enumeration {
	  type VerticalOrientation = Value
	  val Top, VCenter, Bottom = Value
	}

	import VerticalOrientation._

	/**
	 * A composition of horizontal and vertical orientations.
	 */
	class Orientation private (val horizontal: HorizontalOrientation, val vertical: VerticalOrientation)

	object Orientation {
	  def apply (horizontal: HorizontalOrientation, vertical: VerticalOrientation) =
	    new Orientation(horizontal, vertical)
	}

	/** An orientation centred in both directions. */
	final val DefaultOrientation = Orientation(HCenter, VCenter)

  /** `(0, 0)`. */
  final val DefaultOrigin = Pixel(0, 0)

  /**
   * A new cell with given width, height and origin.
   *
   * @param origin the upper-left corner of this cell, defaults to `(0, 0)`
   */
  def apply (width: Int, height: Int, origin: Pixel = DefaultOrigin) =
    new Cell(width, height, origin)

}
