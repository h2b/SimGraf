/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf

/**
 * A package collecting some tools for positioning elements on the screen.
 *
 * It contains a `Cell` (a rectangular area) and a `GridLayout` of cells
 * organized into rows and columns as this:
 *
 * <pre>
 * ----------------------------
 * | cell | cell | cell | ... |
 * ----------------------------
 * | cell | cell | cell | ... |
 * ----------------------------
 * | cell | cell | cell | ... |
 * ----------------------------
 * | ...  | ...  | ...  | ... |
 * ----------------------------
 * </pre>
 *
 * Various factory methods are provided that construct grid layouts from a
 * sample cell, within a cell or on the whole screen.
 *
 * @since 1.1.0
 * @author h2b
 */
package object layout
