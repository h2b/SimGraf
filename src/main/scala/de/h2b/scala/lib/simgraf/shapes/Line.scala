/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.shapes

import de.h2b.scala.lib.simgraf.{ Point, World }

/**
 * A line between two points.
 *
 * @author h2b
 */
case class Line (val p1: Point, val p2: Point) extends Drawable {

  def draw (w: World): Unit = w.screen.drawLine(w.toPixel(p1), w.toPixel(p2))

}
