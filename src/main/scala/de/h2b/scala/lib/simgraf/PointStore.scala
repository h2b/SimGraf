/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf

import de.h2b.scala.lib.math.linalg.{ Index, Vector }
import de.h2b.scala.lib.math.linalg.storage.VectorStore

trait PointStore [E] extends VectorStore[E] {

  def x: E
  def y: E

	val index: Index = Index(1, 2)

  def apply (i: Int): E = i match {
    case 1 ⇒ x
    case 2 ⇒ y
    case _ ⇒ Vector.scal0
  }

  protected val dataHashCode: Int = {
    val prime = 31
    var result = 1
    result = prime*result+x.hashCode()
    result = prime*result+y.hashCode()
    result.toInt
  }

}
