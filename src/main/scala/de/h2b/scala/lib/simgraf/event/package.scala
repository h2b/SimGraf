/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf

import akka.actor.{ ActorSystem, Props }
import akka.event.EventBus

/**
 * This package constitutes a high-level abstraction of input (e.g.,
 * keyboard or mouse) events using an actor event bus.
 *
 * Such events, if triggered by the drivers, are published to the global
 * event stream of this package and can be retrieved by subscribing to the
 * `stream` object.
 *
 * Also, there is a `Subscriber` actor that can be used to handle events.
 * Since version 1.3.0, it's companion object defines `to` methods for screens
 * and worlds with a `PartialFunction[Event, Unit]` as a parameter.
 *
 * So, for example, you just can write
 *
 * {{{
 * 	  Subscriber.to(world) {
 * 	    case e: Event ⇒ println(e)
 * 	  }
 * }}}
 *
 * to print out all events occurring on the specified `world`.
 *
 * To get the triggering enabled, the `withEvents` factory methods of
 * `World` or `Screen` have to be used.
 *
 * Note that the program doesn't terminate by itself on closing all screens if
 * the event package is used. This is due to background processes still
 * listening to the event stream. For explicit termination call the `terminate`
 * method on the package object's `system` value; this can be done, e. g., by
 * subscribing to all screens or worlds and using a termination event like this:
 *
 * {{{
 *	  Subscriber.to(world1, world2) {
 *	    case KeyEvent(k) if k=='q' ⇒
 *	      world1.screen.close()
 *	      world2.screen.close()
 *	      println("terminating...")
 *	      system.terminate()
 *	    case _: Event ⇒ ()
 *	  }
 * }}}
 *
 * @since 1.2.0
 * @author h2b
 */
package object event {

	/**
	 * An event optionally associated with a screen.
	 * @since 1.3.0
	 */
	type ClassifiedEvent = (Event, Option[Screen])

  /** The actor system used in this package. */
  val system = ActorSystem("SimGraf")

	/**
	 * The event stream global to this package.
	 *
	 * Subscribe to this stream to retrieve triggered events.
	 *
	 * @note As of version 1.3.0, the definition changed from `EventStream` to
	 * `ScreenEventBus` (both are implementations of `EventBus`) in order to
	 * handle events classified by screen.
	 */
  val stream = ScreenEventBus

  /**
   * The actor publishing events to the global event stream that are triggered
   * by drivers and are not associated with a screen.
   *
	 * @note As of version 1.3.0, the definition changed from ''all events'' to
	 * ''events not associated with a screen''.
   */
	val publisher = system.actorOf(Props(new Publisher(None)))

}
