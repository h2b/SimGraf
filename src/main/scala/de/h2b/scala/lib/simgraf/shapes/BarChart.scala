/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.shapes

import de.h2b.scala.lib.simgraf.{ Color, ColorIterator, Point, World }

/**
 * A bar chart as shape.
 *
 * Each bar extends from `0` to `h` on the y axis, where `h` is its given
 * height.
 *
 * @constructor
 *
 * @param heights sequence of bar heights
 * @param colors sequence of colors to iterate over for each bar (cycling)
 *
 * @param enc enclosing for this chart; defaults to the full world if not
 * explicitly or otherwise implicitly given
 *
 * @since 1.4.0
 * @author h2b
 */
case class BarChart (val heights: Seq[Double], val colors: Seq[Color])
    (implicit val enc: Enclosing) extends Drawable with Fillable {

	def draw (w: World): Unit =
	  display(w, (p1: Point, p2: Point) ⇒ Rectangle(p1, p2).draw(w))

  def fill (w: World): Unit =
    display(w, (p1: Point, p2: Point) ⇒ Rectangle(p1, p2).fill(w))

  private def display (w: World, f: (Point, Point) ⇒ Unit): Unit = {
	  if (heights.isEmpty) return
	  val activeColor = w.activeColor
    val iterator = ColorIterator.over(colors: _*)
	  val (wl, wr, wb, wt) = enc.lrbt(w)
	  val barWidth = (wr-wl)/heights.size
	  var x = wl
	  for (h ← heights) {
	    w.activeColor = iterator.next()
	    f(Point(x, 0), Point(x+barWidth, h))
	    x += barWidth
	  }
	  w.activeColor = activeColor
	}

}
