/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.shapes

import de.h2b.scala.lib.simgraf.{ Color, ColorIterator, Point, World }

/**
 * A pie chart as shape.
 *
 * The pie is centred at `(0 ,0)` in the enclosing world coordinate system.
 *
 * @constructor
 *
 * @param weights sequence of segment weights
 * @param colors sequence of colors to iterate over for each bar (cycling)
 *
 * @param enc enclosing for this chart; defaults to the full world if not
 * explicitly or otherwise implicitly given
 *
 * @since 1.4.0
 * @author h2b
 */
case class PieChart (val weights: Seq[Double], val colors: Seq[Color])
    (implicit val enc: Enclosing) extends Drawable with Fillable {

	def draw (w: World): Unit =
	  display(w, (alpha1: Double, alpha2: Double) ⇒
	      Arc(Point(0,0), width(w), height(w), alpha1, alpha2).draw(w))

  def fill (w: World): Unit =
	  display(w, (alpha1: Double, alpha2: Double) ⇒
	      Arc(Point(0,0), width(w), height(w), alpha1, alpha2).fill(w))

	private def width (w: World): Double = {
	  val (wl, wr, wb, wt) = enc.lrbt(w)
	  wr-wl
	}

	private def height (w: World): Double = {
	  val (wl, wr, wb, wt) = enc.lrbt(w)
	  wt-wb
	}

  private def display (w: World, f: (Double, Double) ⇒ Unit): Unit = {
	  if (weights.isEmpty) return
	  val activeColor = w.activeColor
    val iterator = ColorIterator.over(colors: _*)
	  val sum = weights.sum
	  val angles = weights map { _*2*Math.PI/sum }
	  val r1 = width(w)/2
	  val r2 = height(w)/2
	  val p0 = Point(0, 0)
	  var alpha = 0.0
	  //Lines are for arc drawing and do not disturb arc filling
	  for (a ← angles) {
	    w.activeColor = iterator.next()
	    w.draw(Line(p0, Point(r1*Math.cos(alpha), r2*Math.sin(alpha))))
	    f(alpha, a)
	    alpha += a
	    w.draw(Line(p0, Point(r1*Math.cos(alpha), r2*Math.sin(alpha))))
	  }
	  w.activeColor = activeColor
	}

}
