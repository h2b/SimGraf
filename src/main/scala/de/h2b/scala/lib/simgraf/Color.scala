/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf

/**
 * @author h2b
 *
 * @constructor
 *
 * @param red component of the RGB space (0..255)
 * @param green component of the RGB space (0..255)
 * @param blue component of the RGB space (0..255)
 * @param alpha optional transparency value (0..255, defaults to 255, i.e., opaque)
 *
 * @throws IllegalArgumentException if a parameter is not in the interval `[0, 255]`
 */
case class Color (red: Int, green: Int, blue: Int, alpha: Int = 255) {

  require(0<=red && red<=255, s"red component out of range: $red")
  require(0<=green && green<=255, s"green component out of range: $green")
  require(0<=blue && blue<=255, s"blue component out of range: $blue")
  require(0<=alpha && alpha<=255, s"alpha component out of range: $alpha")

}

object Color {

	final val Black = Color(0, 0, 0)
	final val Blue = Color(0, 0, 255)
	final val Red = Color(255, 0, 0)
	final val Green = Color(0, 255, 0)
	final val Gray = Color(128, 128, 128)
	final val Cyan = Color(0, 255, 255)
	final val Magenta = Color(255, 0, 255)
	final val Yellow = Color(255, 255, 0)
	final val DarkGray = Color(64, 64, 64)
	final val Orange = Color(255, 200, 0)
	final val Pink = Color(255, 175, 175)
	final val LightGray = Color(192, 192, 192)
	final val White = Color(255, 255, 255)

}