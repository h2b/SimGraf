/*
  SimGraf - A Simple Scala Graphics Library

  Copyright 2016-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.simgraf.shapes.parallel

import scala.concurrent.Future

import de.h2b.scala.lib.simgraf.{ Color, Pixel, Point, World }
import de.h2b.scala.lib.simgraf.shapes.{ Enclosing, Fillable }

/**
 * A computed coloring using parallel operations.
 *
 * @constructor
 *
 * @param col computing function that maps point coordinates to colors
 *
 * @param enc enclosing for this grid; defaults to the full world if not
 * explicitly or otherwise implicitly given
 *
 * @since 1.1.0
 * @author h2b
 */
case class Coloring (val col: Point ⇒ Color) (implicit val enc: Enclosing)
    extends Fillable with FutureTracking {

  def fill (w: World): Unit = {
    val bounds = enc.bounds(w)
    def pixcol (p: Pixel): Color = col(w.toPoint(p))
    future = w.screen.setPixelsParallel(w.toPixel(bounds.p1), w.toPixel(bounds.p2))(pixcol)
  }

}
