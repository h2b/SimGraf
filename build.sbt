name := "SimGraf"

scalaVersion := "2.12.5"

libraryDependencies ++= Seq(
	"de.h2b.scala.lib.math" %% "linalg" % "3.1.0",
	"de.h2b.scala.lib" %% "utilib" % "0.4.1",
	"com.typesafe.akka" %% "akka-actor" % "2.5.11",
	"org.scalatest" %% "scalatest" % "3.0.5" % Test,
	"junit" % "junit" % "4.12" % Test
)
